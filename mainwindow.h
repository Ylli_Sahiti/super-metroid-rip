#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>

#include <QFileDialog>


#include <QGraphicsView>
#include <QGraphicsLineItem>
#include <QLabel>

#include <QtMath>
#include <QVector>

#include "tile.h"
#include "worldmap.h"
#include "globals.h"

#include <QEvent>
#include <QMouseEvent>
#include <QKeyEvent>

#include <producer.h>

#include <QMutex>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void testFunction() {

        tileToInsert = new QImage("images/sprites/a.png");

        QImage *image = new QImage(tileToInsert->width(), tileToInsert->height(), QImage::Format_ARGB32);

        for (int x = 0; x < image->width(); ++x) {
            for (int y = 0; y < image->height(); ++y) {
                image->setPixelColor(x, y, tileToInsert->pixelColor(x, y));
            }
        }
        delete tileToInsert;
        tileToInsert = image;

        makeTransparent(tileToInsert);
    }

    void testFunction2() {

        QImage *first = new QImage("C:/Users/Sahiti Corp/Desktop/Metroid/TileRipper/images/a.png");
        QImage *second = new QImage("C:/Users/Sahiti Corp/Desktop/Metroid/TileRipper/images/b.png");

        for (int x = 0; x < first->width(); x++) {

            for (int y = 0; y < first->height(); y++) {
                QColor col1 = first->pixelColor(x, y);
                QColor col2 = second->pixelColor(x, y);
                if (col1 != col2) {
                    qDebug() << "Global colors: " << col1.red() << col1.green() << col1.blue() << col1.alpha();
                    qDebug() << "Image Tile colors: " << col2.red() << col2.green() << col2.blue() << col2.alpha();
                }
            }
        }
    }

    void makeTransparent(QImage *image) {
        for (int x = 0; x < image->width(); ++x) {
            for (int y = 0; y < image->height(); ++y) {
                if (image->pixelColor(x, y) ==  transparentColor) {
                    image->setPixelColor(x, y, transparentColorWithAlpha);
                }
            }
        }
    }

    bool addUniqueImageInVector(QVector<QImage *> &images, QImage *image);

    void connectZoneCheckboxes();
    void zoneNoneToggled();

    void connectMiscCheckboxes();
    void miscNoneToggled();

    void calculateTickedTileTypes();

    void btnLoadIconPressed();
    void btnDeleteIconPressed();

    void mousePressEvent(QMouseEvent *event);

    void setTileFromFile(const QPoint &scenePos);

    void setEntireImage(const QPoint &scenePos);

    void setEntireF(const QPoint &scenePos);

    bool eventFilter(QObject* watched, QEvent* event);

    void makeBackground();

    void deleteSelected();

    void invalidate();

    void keyPressEvent(QKeyEvent *e);

    QString getFilterName(TileType zone);
    void loadCurrentSprites();
    void loadWorldmapMatrix();

    void createPNGBackgroundTile();

    void convertToPNG();

    void ripTiles();
    void ripLeft();
    void ripRight();

    void makeImageOfMapPressed();

    void loadWorldmap();
    void matchScreenToWorldmap();
    void matchTilesToWorldmap();
    bool matchSpecificWorldmapTile(TileType zone, const QPoint &scenePos);
    void highlightMatch(const QPoint &scenePos);
    bool areSame(QPoint scenePos, QImage *tileToInsert);

    bool zoneIsActive(TileType zone);

    void cropImages();
    void removeOutOfBoundsImages();
    bool isOutOfBoundsTile(QImage *tileToInsert);
    bool isOutOfBoundsTile(const QPoint &scenePos);
    bool areDuplicates(QImage *src, QImage *dest);
    void removeDuplicates();

    bool areSame(QImage *src, QImage *dest);
    void changeTileOfWorldmapFromImage(const QPoint &scenePos);
    void saveWorldmapChangesPushed();

    void makeImageFromMapTile(const QPoint &scenePos, QImage *image);

    void saveImages();
    QStringList getTileFileNames(QString path);
    void saveMatrix();
    void showWorldmap(bool show);
    void makeBlack(bool value);

    TileType activeTileTypes();

public slots:
    void newItemAdded() {
        mutex.lock();
        QList<Tile *> tempList = tilesToAdd;
        tilesToAdd.clear();
        mutex.unlock();

        for (Tile *tile : tempList) {
            scene->addItem(tile);
        }
    }
    void threadFinished(Producer *thread) {
        newItemAdded();

        this->producerThreads.removeOne(thread);
        thread->deleteLater();
    }

private:
    Ui::MainWindow *ui;

    QList<Producer *> producerThreads;

    QMap<TileType, QVector<QImage *> > zoneIdImageMap;
    QVector<QImage *> images;

    QList<Tile *> tilesToAdd;

    QImage *imageToRip;
    WorldMap *worldMap;

    int highestID = -1;

    QGraphicsView *view;
    QGraphicsScene *scene;

    QImage *tileToInsert = nullptr;

    QMutex mutex;

    QColor transparentColor = QColor(1, 1, 1);
    QColor transparentColorWithAlpha = QColor(0, 0, 0, 0);

};


#endif // MAINWINDOW_H
