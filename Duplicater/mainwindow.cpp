#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QList>
#include <QImage>
#include <QCollator>
#include <QDirIterator>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);



    initMock();

//    runMaintenance();

//    runCheckIsPowered();

//    runPrintIndexOfImages();

//    runDetectColorPalette();

    runDefaultFunctionality();
}

void MainWindow::loadSelectedFolder() {

    folderPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                   "C:/Users/Sahiti Corp/Desktop/Metroid/TileRipper/images/sprites/tile_types/zones/",
                                                   QFileDialog::ShowDirsOnly
                                                   | QFileDialog::DontResolveSymlinks);

    QStringList tileFileNames = getTileFileNamesSorted(folderPath);

    for (QString fileName : tileFileNames) {
        imagesNew.push_back(new Tile(new QImage(folderPath + "/" + fileName)));
    }
}

void MainWindow::runMaintenance() {

    loadSelectedFolder();

    groupTransformations();
    groupSimilar(imagesNew);
    sortImages(imagesNew);

    int i = 1;
    for (Tile *image : imagesNew) {
        image->save(folderPath + "/sorted/" + QString::number(i++) + ".png");
    }

}

void MainWindow::runCheckIsPowered() {

    loadSelectedFolder();

    for (Tile *image : qAsConst(imagesNew)) {

        bool isPowered = isAffectedTile(image);
        qDebug() << isPowered;
    }
}

void MainWindow::runPrintIndexOfImages() {

    loadSelectedFolder();

    int indexAt = -1;
    for (Tile *image : qAsConst(imagesNew)) {
        indexAt = getImageFireIndex(image);
        qDebug() << indexAt;
    }

    exit(0);
}

void MainWindow::runDetectColorPalette() {

    loadSelectedFolder();

    QVector<QVector<QColor>> allColorPalettes;

    for (int x = 0; x < imagesNew.first()->width(); x++) {
        for (int y = 0; y < imagesNew.first()->height(); y++) {
            allColorPalettes.push_back(QVector<QColor>());

            QVector<QColor> &currentPalette = allColorPalettes.last();

            for (Tile *image : imagesNew) {
                currentPalette.push_back(image->pixelColor(x, y));
            }
        }
    }

    QVector<QColor> defaultColors;

    for (int i = 0; i < allColorPalettes.size(); i++) {
        QVector<QColor> colorPalette = allColorPalettes.at(i);

        const QColor firstColor = colorPalette.first();
        for (int index = 1; index < colorPalette.size(); index++) {
            if (firstColor != colorPalette.at(index)) goto endOfLoop;
        }

        if (!defaultColors.contains(firstColor)) {
            defaultColors.push_back(firstColor);
        }
        allColorPalettes.removeAt(i);
        i--;
        continue;

        endOfLoop: ;
    }


    for (int first = 0; first < allColorPalettes.size() - 1; first++) {
        QVector<QColor> firstColorPalette = allColorPalettes.at(first);

        for (int second = first + 1; second < allColorPalettes.size(); second++) {
            QVector<QColor> secondColorPalette = allColorPalettes.at(second);

            bool areIdenticalPalettes = true;
            for (int i = 0; i < firstColorPalette.size(); i++) {
                if (firstColorPalette.at(i) != secondColorPalette.at(i)) {
                    areIdenticalPalettes = false;
                    break;
                }
            }
            if (areIdenticalPalettes) {
                allColorPalettes.removeAt(second);
                second--;
            }
        }
    }

    qDebug() << "asserts.clear();";
    qDebug() << "materialColors.clear();";
    qDebug() << "materialLevelColors.clear();" << Qt::endl;

    for (int i = 0; i < defaultColors.size(); i++) {
        QString message = "";
        QColor color = defaultColors.at(i);
        message += "Colors.push_back(QColor(";
        message += QString::number(color.red()) + ", ";
        message += QString::number(color.green()) + ", ";
        message += QString::number(color.blue()) + ", ";
        message += QString::number(color.alpha()) + "));";

        qDebug() << message;
    }

    for (QVector<QColor> &colorPalette : allColorPalettes) {

        qDebug() << Qt::endl << "materialLevelColors.push_back(QVector<QColor>());";
        QString colorPal = "";
        for (QColor &color : colorPalette) {
            colorPal += "materialLevelColors.back().push_back(QColor(";
            colorPal += QString::number(color.red()) + ", ";
            colorPal += QString::number(color.green()) + ", ";
            colorPal += QString::number(color.blue()) + "));";
        }

        qDebug() << colorPal;
    }

    qDebug() << Qt::endl << "materialColorPalettes.push_back(qMakePair(Colors, GlowingLevelColors));" << Qt::endl;

    printAssert(defaultColors, allColorPalettes);
}

void MainWindow::runDefaultFunctionality() {



    folderPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    "C:/Users/Sahiti Corp/Desktop/metroid/TileRipper/images/sprites",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);

    loadImages(folderPath);

    groupSimilar(imagesNew);
    sortImages(imagesNew);

    addRotatedImages();

    addFlippedImages(true, true);

    generateFireTiles();

    generateUnaffectedTiles();

    removeDuplicates(imagesNew);

    saveAllImages(imagesNew, folderPath);
    exit(0);
}

QString MainWindow::getLevelString(int level) {
    switch ((Levels)level) {
    case ZERO: return "0";
    case ONE: return "1";
    case TWO: return "2";
    case THREE: return "3";
    case FOUR: return "4";
    case FIVE: return "5";
    case SIX: return "6";
    case SEVEN: return "7";
    case EIGHT: return "8";
    case ROCK_NORMAL: return "rock_normal";
    case ROCK_MOLTEN: return "rock_molten";
    case UNAFFECTED: return "unaffected";
    case NORMAL: return "normal";
    }
}

void MainWindow::initMock() {
    //
    rockNormalColors.push_back(transparentColorWithAlpha);
    rockNormalColors.push_back(QColor(  8,  16,  24));
    rockNormalColors.push_back(QColor(104,  56,  56));
    rockNormalColors.push_back(QColor(160, 112, 112));
    rockNormalColors.push_back(QColor(200, 152, 152));

    rockMoltenColors.push_back(transparentColorWithAlpha);
    rockMoltenColors.push_back(QColor(  8,  16,  24));
    rockMoltenColors.push_back(QColor( 72,  16,  24));
    rockMoltenColors.push_back(QColor(144,  24,  24));
    rockMoltenColors.push_back(QColor(200,  32,  24));

    rockMoltenLevelColors.push_back(QColor(184,  24,  24));
    rockMoltenLevelColors.push_back(QColor(192,  40,  24));
    rockMoltenLevelColors.push_back(QColor(200,  64,  32));
    rockMoltenLevelColors.push_back(QColor(208,  88,  40));
    rockMoltenLevelColors.push_back(QColor(216, 104,  40));
    rockMoltenLevelColors.push_back(QColor(224, 128,  48));
    rockMoltenLevelColors.push_back(QColor(232, 152,  56));
    rockMoltenLevelColors.push_back(QColor(248, 184,  64));
    //

    //
    QVector<int> asserts;
    QVector<QColor> materialColors;
    QVector<QVector<QColor>> materialLevelColors;

    asserts.push_back(0);
    asserts.push_back(1);
    allAsserts.push_back(asserts);

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor(  0,   0,   0));
    materialColors.push_back(QColor( 32,  32,  32));
    materialColors.push_back(QColor( 88,  88,  88));
    materialColors.push_back(QColor(144, 144, 144));
    materialColors.push_back(QColor(184, 184, 184));
    materialColors.push_back(QColor(248, 176,  24));
    materialColors.push_back(QColor(248, 248, 248));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(144, 144, 144));
    materialLevelColors.back().push_back(QColor(144, 144, 144));
    materialLevelColors.back().push_back(QColor(160, 128, 128));
    materialLevelColors.back().push_back(QColor(168, 120, 112));
    materialLevelColors.back().push_back(QColor(184, 112,  96));
    materialLevelColors.back().push_back(QColor(200, 112,  80));
    materialLevelColors.back().push_back(QColor(208, 104,  64));
    materialLevelColors.back().push_back(QColor(224,  96,  48));
    materialLevelColors.back().push_back(QColor(248,  88,  24));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  32,  24));
    materialLevelColors.back().push_back(QColor(200,  16,   8));
    materialLevelColors.back().push_back(QColor(208,  32,  16));
    materialLevelColors.back().push_back(QColor(216,  56,  16));
    materialLevelColors.back().push_back(QColor(224,  72,  24));
    materialLevelColors.back().push_back(QColor(224,  96,  32));
    materialLevelColors.back().push_back(QColor(232, 112,  40));
    materialLevelColors.back().push_back(QColor(240, 136,  40));
    materialLevelColors.back().push_back(QColor(248, 152,  48));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  88,  24));
    materialLevelColors.back().push_back(QColor(216,  72,  16));
    materialLevelColors.back().push_back(QColor(224,  88,  24));
    materialLevelColors.back().push_back(QColor(224, 104,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  32));
    materialLevelColors.back().push_back(QColor(232, 128,  40));
    materialLevelColors.back().push_back(QColor(240, 144,  48));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(248, 176,  56));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248, 136,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  16));
    materialLevelColors.back().push_back(QColor(232, 136,  24));
    materialLevelColors.back().push_back(QColor(240, 144,  40));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(240, 168,  56));
    materialLevelColors.back().push_back(QColor(240, 184,  64));
    materialLevelColors.back().push_back(QColor(248, 192,  80));
    materialLevelColors.back().push_back(QColor(248, 208,  88));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));
    //

    //
    asserts.clear();
    materialColors.clear();
    materialLevelColors.clear();

    asserts.push_back(-1);
    allAsserts.push_back(asserts);

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor(  0,   0,   0));
    materialColors.push_back(QColor( 32,  32,  32));
    materialColors.push_back(QColor( 96,  72,  64));
    materialColors.push_back(QColor(152, 104,  88));
    materialColors.push_back(QColor(208, 136, 112));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  32,  24));
    materialLevelColors.back().push_back(QColor(200,  16,   8));
    materialLevelColors.back().push_back(QColor(208,  32,  16));
    materialLevelColors.back().push_back(QColor(216,  56,  16));
    materialLevelColors.back().push_back(QColor(224,  72,  24));
    materialLevelColors.back().push_back(QColor(224,  96,  32));
    materialLevelColors.back().push_back(QColor(232, 112,  40));
    materialLevelColors.back().push_back(QColor(240, 136,  40));
    materialLevelColors.back().push_back(QColor(248, 152,  48));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  88,  24));
    materialLevelColors.back().push_back(QColor(216,  72,  16));
    materialLevelColors.back().push_back(QColor(224,  88,  24));
    materialLevelColors.back().push_back(QColor(224, 104,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  32));
    materialLevelColors.back().push_back(QColor(232, 128,  40));
    materialLevelColors.back().push_back(QColor(240, 144,  48));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(248, 176,  56));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));
    //

    // crystals
    asserts.clear();
    materialColors.clear();
    materialLevelColors.clear();

    asserts.push_back(0);
    asserts.push_back(1);
    allAsserts.push_back(asserts);

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor(  0,   0,   0));
    materialColors.push_back(QColor(  8,  16,  24));
    materialColors.push_back(QColor( 16,  48,  32));
    materialColors.push_back(QColor( 40, 112,  72));
    materialColors.push_back(QColor( 72,  16,  24));
    materialColors.push_back(QColor( 72, 192, 128));
    materialColors.push_back(QColor( 96, 248, 160));
    materialColors.push_back(QColor(144,  24,  24));
    materialColors.push_back(QColor(200,  32,  24));
    materialColors.push_back(QColor(248, 248, 248));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor( 72, 192, 128));
    materialLevelColors.back().push_back(QColor( 72, 192, 128));
    materialLevelColors.back().push_back(QColor( 96, 168, 104));
    materialLevelColors.back().push_back(QColor(120, 160,  96));
    materialLevelColors.back().push_back(QColor(144, 144,  80));
    materialLevelColors.back().push_back(QColor(160, 128,  64));
    materialLevelColors.back().push_back(QColor(184, 120,  56));
    materialLevelColors.back().push_back(QColor(208, 104,  40));
    materialLevelColors.back().push_back(QColor(248,  88,  24));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  32,  24));
    materialLevelColors.back().push_back(QColor(200,  16,   8));
    materialLevelColors.back().push_back(QColor(208,  32,  16));
    materialLevelColors.back().push_back(QColor(216,  56,  16));
    materialLevelColors.back().push_back(QColor(224,  72,  24));
    materialLevelColors.back().push_back(QColor(224,  96,  32));
    materialLevelColors.back().push_back(QColor(232, 112,  40));
    materialLevelColors.back().push_back(QColor(240, 136,  40));
    materialLevelColors.back().push_back(QColor(248, 152,  48));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  88,  24));
    materialLevelColors.back().push_back(QColor(216,  72,  16));
    materialLevelColors.back().push_back(QColor(224,  88,  24));
    materialLevelColors.back().push_back(QColor(224, 104,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  32));
    materialLevelColors.back().push_back(QColor(232, 128,  40));
    materialLevelColors.back().push_back(QColor(240, 144,  48));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(248, 176,  56));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(184,  24,  24));
    materialLevelColors.back().push_back(QColor(184,  24,  24));
    materialLevelColors.back().push_back(QColor(192,  40,  24));
    materialLevelColors.back().push_back(QColor(200,  64,  32));
    materialLevelColors.back().push_back(QColor(208,  88,  40));
    materialLevelColors.back().push_back(QColor(216, 104,  40));
    materialLevelColors.back().push_back(QColor(224, 128,  48));
    materialLevelColors.back().push_back(QColor(232, 152,  56));
    materialLevelColors.back().push_back(QColor(248, 184,  64));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248, 136,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  16));
    materialLevelColors.back().push_back(QColor(232, 136,  24));
    materialLevelColors.back().push_back(QColor(240, 144,  40));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(240, 168,  56));
    materialLevelColors.back().push_back(QColor(240, 184,  64));
    materialLevelColors.back().push_back(QColor(248, 192,  80));
    materialLevelColors.back().push_back(QColor(248, 208,  88));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));
     // crystals
    
    //
    
    asserts.clear();
    materialColors.clear();
    materialLevelColors.clear();

    asserts.push_back(0);
    asserts.push_back(1);
    asserts.push_back(8);
    allAsserts.push_back(asserts);

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor(  0,   0,   0));
    materialColors.push_back(QColor( 24,   0,   8));
    materialColors.push_back(QColor( 32,   0,   8));
    materialColors.push_back(QColor( 32,  32,  32));
    materialColors.push_back(QColor( 64,   0,  24));
    materialColors.push_back(QColor( 72,  24,  56));
    materialColors.push_back(QColor( 96,   0,  40));
    materialColors.push_back(QColor( 96,  72,  64));
    materialColors.push_back(QColor(120,  48, 112));
    materialColors.push_back(QColor(152, 104,  88));
    materialColors.push_back(QColor(168,  80, 168));
    materialColors.push_back(QColor(208, 136, 112));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(152, 104,  88));
    materialLevelColors.back().push_back(QColor(152, 104,  88));
    materialLevelColors.back().push_back(QColor(160,  96,  72));
    materialLevelColors.back().push_back(QColor(176,  88,  64));
    materialLevelColors.back().push_back(QColor(184,  80,  56));
    materialLevelColors.back().push_back(QColor(200,  80,  48));
    materialLevelColors.back().push_back(QColor(216,  72,  40));
    materialLevelColors.back().push_back(QColor(224,  64,  32));
    materialLevelColors.back().push_back(QColor(248,  64,  24));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(120,  48, 112));
    materialLevelColors.back().push_back(QColor(120,  48, 112));
    materialLevelColors.back().push_back(QColor(136,  48,  96));
    materialLevelColors.back().push_back(QColor(152,  48,  80));
    materialLevelColors.back().push_back(QColor(168,  48,  72));
    materialLevelColors.back().push_back(QColor(184,  56,  64));
    materialLevelColors.back().push_back(QColor(200,  56,  48));
    materialLevelColors.back().push_back(QColor(216,  56,  40));
    materialLevelColors.back().push_back(QColor(248,  64,  24));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  88,  24));
    materialLevelColors.back().push_back(QColor(216,  72,  16));
    materialLevelColors.back().push_back(QColor(224,  88,  24));
    materialLevelColors.back().push_back(QColor(224, 104,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  32));
    materialLevelColors.back().push_back(QColor(232, 128,  40));
    materialLevelColors.back().push_back(QColor(240, 144,  48));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(248, 176,  56));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  32,  24));
    materialLevelColors.back().push_back(QColor(200,  16,   8));
    materialLevelColors.back().push_back(QColor(208,  32,  16));
    materialLevelColors.back().push_back(QColor(216,  56,  16));
    materialLevelColors.back().push_back(QColor(224,  72,  24));
    materialLevelColors.back().push_back(QColor(224,  96,  32));
    materialLevelColors.back().push_back(QColor(232, 112,  40));
    materialLevelColors.back().push_back(QColor(240, 136,  40));
    materialLevelColors.back().push_back(QColor(248, 152,  48));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248, 136,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  16));
    materialLevelColors.back().push_back(QColor(232, 136,  24));
    materialLevelColors.back().push_back(QColor(240, 144,  40));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(240, 168,  56));
    materialLevelColors.back().push_back(QColor(240, 184,  64));
    materialLevelColors.back().push_back(QColor(248, 192,  80));
    materialLevelColors.back().push_back(QColor(248, 208,  88));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));

    //

    asserts.clear();
    materialColors.clear();
    materialLevelColors.clear();

    asserts.push_back(-1);
    allAsserts.push_back(asserts);

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor( 24,  64, 152));
    materialColors.push_back(QColor( 48,  32,  40));
    materialColors.push_back(QColor( 56, 112, 224));
    materialColors.push_back(QColor(112,  72,  80));
    materialColors.push_back(QColor(144, 168, 224));
    materialColors.push_back(QColor(152, 104, 120));
    materialColors.push_back(QColor(216, 168, 184));
    materialColors.push_back(QColor(248, 248, 248));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  32,  24));
    materialLevelColors.back().push_back(QColor(200,  16,   8));
    materialLevelColors.back().push_back(QColor(208,  32,  16));
    materialLevelColors.back().push_back(QColor(216,  56,  16));
    materialLevelColors.back().push_back(QColor(224,  72,  24));
    materialLevelColors.back().push_back(QColor(224,  96,  32));
    materialLevelColors.back().push_back(QColor(232, 112,  40));
    materialLevelColors.back().push_back(QColor(240, 136,  40));
    materialLevelColors.back().push_back(QColor(248, 152,  48));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));
    //

    // wrecked ship unpowred to powered
    affectedTileColors.push_back(transparentColorWithAlpha);
    affectedTileColors.push_back(QColor(  0,   0,   0));
    affectedTileColors.push_back(QColor(  0,  48,   0));
    affectedTileColors.push_back(QColor( 24, 120,   8));
    affectedTileColors.push_back(QColor( 32,  32,   8));
    affectedTileColors.push_back(QColor( 32,  32,  40));
    affectedTileColors.push_back(QColor( 32,  32,  88));
    affectedTileColors.push_back(QColor( 40,  80,   0));
    affectedTileColors.push_back(QColor( 48,  48,   8));
    affectedTileColors.push_back(QColor( 64,  64,  24));
    affectedTileColors.push_back(QColor( 64,  64, 120));
    affectedTileColors.push_back(QColor( 64,  88,  24));
    affectedTileColors.push_back(QColor( 64, 160,   0));
    affectedTileColors.push_back(QColor( 72, 168,  56));
    affectedTileColors.push_back(QColor( 96,  96,  56));
    affectedTileColors.push_back(QColor(112, 112,  72));
    affectedTileColors.push_back(QColor(112, 112, 168));
    affectedTileColors.push_back(QColor(112, 136,  72));
    affectedTileColors.push_back(QColor(136, 232,  16));
    affectedTileColors.push_back(QColor(160, 160, 120));
    affectedTileColors.push_back(QColor(160, 160, 216));
    affectedTileColors.push_back(QColor(160, 184, 120));
    affectedTileColors.push_back(QColor(248, 248, 248));

    unaffectedTileColors.push_back(transparentColorWithAlpha);
    unaffectedTileColors.push_back(QColor(  0,   0,   0));
    unaffectedTileColors.push_back(QColor(  0,   8,   0));
    unaffectedTileColors.push_back(QColor(  8,   8,  64));
    unaffectedTileColors.push_back(QColor(  0,   0,   0));
    unaffectedTileColors.push_back(QColor(  8,   8,   0));
    unaffectedTileColors.push_back(QColor(  0,   0,  32));
    unaffectedTileColors.push_back(QColor( 24,  64,   0));
    unaffectedTileColors.push_back(QColor( 16,  16,   0));
    unaffectedTileColors.push_back(QColor( 24,  24,   0));
    unaffectedTileColors.push_back(QColor(  8,   8,  64));
    unaffectedTileColors.push_back(QColor(  8,  32,   0));
    unaffectedTileColors.push_back(QColor( 56,  56, 112));
    unaffectedTileColors.push_back(QColor(  8,   8,  64));
    unaffectedTileColors.push_back(QColor( 56,  56,  16));
    unaffectedTileColors.push_back(QColor( 56,  56,  24));
    unaffectedTileColors.push_back(QColor( 56,  56, 112));
    unaffectedTileColors.push_back(QColor( 40,  64,   0));
    unaffectedTileColors.push_back(QColor( 56,  56, 112));
    unaffectedTileColors.push_back(QColor( 80,  80,  40));
    unaffectedTileColors.push_back(QColor(104, 104, 168));
    unaffectedTileColors.push_back(QColor( 80, 104,  40));
    unaffectedTileColors.push_back(QColor(144, 144, 144));
    // wrecked ship unpowred to powered

    // Norfair lava affected to unaffected
    affectedTileColors.push_back(QColor(  0,   0,   0, 0));
    affectedTileColors.push_back(QColor(  0,   0,   0));
    affectedTileColors.push_back(QColor( 32,  32, 32));
    affectedTileColors.push_back(QColor( 96,  72, 64));
    affectedTileColors.push_back(QColor(152, 104, 88));
    affectedTileColors.push_back(QColor(208, 136, 112));
    affectedTileColors.push_back(QColor(248,  64,  24));
    affectedTileColors.push_back(QColor(248, 152, 48));
    affectedTileColors.push_back(QColor(248, 176, 56));

    unaffectedTileColors.push_back(QColor(  0,   0,   0, 0));
    unaffectedTileColors.push_back(QColor(  0,   0,   0));
    unaffectedTileColors.push_back(QColor( 48,  32, 40));
    unaffectedTileColors.push_back(QColor(112,  72, 80));
    unaffectedTileColors.push_back(QColor(152, 104, 120));
    unaffectedTileColors.push_back(QColor(216, 168, 184));
    unaffectedTileColors.push_back(QColor(184, 160, 104));
    unaffectedTileColors.push_back(QColor(176,   0, 48));
    unaffectedTileColors.push_back(QColor(216,  56, 144));
    // Norfair lava affected to unaffected

    //
    asserts.clear();
    materialColors.clear();
    materialLevelColors.clear();

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor(  0,   0,   0));
    materialColors.push_back(QColor( 32,  32,  40));
    materialColors.push_back(QColor( 32,  32,  88));
    materialColors.push_back(QColor( 64,  64,  24));
    materialColors.push_back(QColor( 64,  64, 120));
    materialColors.push_back(QColor( 64,  88,  24));
    materialColors.push_back(QColor(112, 112,  72));
    materialColors.push_back(QColor(112, 112, 168));
    materialColors.push_back(QColor(112, 136,  72));
    materialColors.push_back(QColor(160, 160, 120));
    materialColors.push_back(QColor(160, 184, 120));
    materialColors.push_back(QColor(248, 248, 248));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor( 40, 136,   0));
    materialLevelColors.back().push_back(QColor( 64, 160,   0));
    materialLevelColors.back().push_back(QColor( 88, 184,   0));
    materialLevelColors.back().push_back(QColor(112, 208,   0));
    materialLevelColors.back().push_back(QColor(136, 232,  16));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(  8, 104,   0));
    materialLevelColors.back().push_back(QColor( 24, 120,   8));
    materialLevelColors.back().push_back(QColor( 40, 136,  24));
    materialLevelColors.back().push_back(QColor( 56, 152,  40));
    materialLevelColors.back().push_back(QColor( 72, 168,  56));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));

    asserts.push_back(-1);
    allAsserts.push_back(asserts);
    //

    // Maridia
    // pipes
    asserts.clear();
    materialColors.clear();
    materialLevelColors.clear();

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor(  0,   0,   0));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(32, 32, 48));
    materialLevelColors.back().push_back(QColor(56, 24, 40));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(56, 56, 72));
    materialLevelColors.back().push_back(QColor(104, 40, 88));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(88, 88, 104));
    materialLevelColors.back().push_back(QColor(160, 72, 144));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(120, 120, 136));
    materialLevelColors.back().push_back(QColor(208, 120, 208));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(216, 216, 216));
    materialLevelColors.back().push_back(QColor(248, 248, 248));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));

    asserts.push_back(-1);
    allAsserts.push_back(asserts);
    // Maridia
    // pipes

    asserts.clear();
    materialColors.clear();
    materialLevelColors.clear();

    asserts.push_back(-1);
    allAsserts.push_back(asserts);

    materialColors.push_back(transparentColorWithAlpha);
    materialColors.push_back(QColor(  0,   0,   0));
    materialColors.push_back(QColor( 24,  64, 152));
    materialColors.push_back(QColor( 32,  32,  32));
    materialColors.push_back(QColor( 56, 112, 224));
    materialColors.push_back(QColor( 88,  88,  88));
    materialColors.push_back(QColor(144, 144, 144));
    materialColors.push_back(QColor(184, 184, 184));
    materialColors.push_back(QColor(248, 248, 248));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  32,  24));
    materialLevelColors.back().push_back(QColor(200,  16,   8));
    materialLevelColors.back().push_back(QColor(208,  32,  16));
    materialLevelColors.back().push_back(QColor(216,  56,  16));
    materialLevelColors.back().push_back(QColor(224,  72,  24));
    materialLevelColors.back().push_back(QColor(224,  96,  32));
    materialLevelColors.back().push_back(QColor(232, 112,  40));
    materialLevelColors.back().push_back(QColor(240, 136,  40));
    materialLevelColors.back().push_back(QColor(248, 152,  48));

    materialLevelColors.push_back(QVector<QColor>());
    materialLevelColors.back().push_back(QColor(248,  88,  24));
    materialLevelColors.back().push_back(QColor(216,  72,  16));
    materialLevelColors.back().push_back(QColor(224,  88,  24));
    materialLevelColors.back().push_back(QColor(224, 104,  24));
    materialLevelColors.back().push_back(QColor(232, 120,  32));
    materialLevelColors.back().push_back(QColor(232, 128,  40));
    materialLevelColors.back().push_back(QColor(240, 144,  48));
    materialLevelColors.back().push_back(QColor(240, 160,  48));
    materialLevelColors.back().push_back(QColor(248, 176,  56));

    materialColorPalettes.push_back(qMakePair(materialColors, materialLevelColors));

    //

}

void MainWindow::printAssert(QVector<QColor> &fireColors, QVector<QVector<QColor>> &fireLevels) {

    QVector<int> indexToAvoid;

    for (int firstPaletteIndex = 0; firstPaletteIndex < fireLevels.size() - 1; firstPaletteIndex++) {
        QVector<QColor> firstPalette = fireLevels.at(firstPaletteIndex);

        for (int secondPaletteIndex = firstPaletteIndex + 1; secondPaletteIndex < fireLevels.size(); secondPaletteIndex++) {
            QVector<QColor> secondPalette = fireLevels.at(secondPaletteIndex);

            for (int index = 0; index < fireLevels.at(firstPaletteIndex).size(); index++) {

                if (firstPalette.at(index) == secondPalette.at(index)) {
                    if (!indexToAvoid.contains(index)) indexToAvoid.push_back(index);
                }
            }
        }
    }

    for (QColor color : fireColors) {

        for (int paletteIndex = 0; paletteIndex < fireLevels.size(); paletteIndex++) {
            QVector<QColor> palette = fireLevels.at(paletteIndex);

            for (int index = 0; index < palette.size(); index++) {

                if (color == palette.at(index)) {

                    if (!indexToAvoid.contains(index)) indexToAvoid.push_back(index);
                }
            }
        }
    }

    if (indexToAvoid.isEmpty()) indexToAvoid.push_back(-1);
    for (int index : indexToAvoid) {
        qDebug() << "asserts.push_back(" + QString::number(index) + ");";
    }
    qDebug() << "allAsserts.push_back(asserts);" << Qt::endl;

    exit(0);
}

void MainWindow::loadImages(QString &folderPath) {

    for (int level = Levels::ONE; level <= Levels::TWO; level++) {
        QString levelPath = getLevelString(level);

        QStringList tileFileNames = getTileFileNamesSorted(folderPath + "/" + levelPath);
        for (QString fileName : tileFileNames) {
            Tile *image = new Tile(new QImage(folderPath + "/" + levelPath + "/" + fileName));
            image->level = level;

            push_back_unique(imagesExisting, image);
        }
    }

    QDirIterator it(folderPath + "/images", QDirIterator::Subdirectories);

    while (!it.next().isEmpty()) {
        if (it.fileInfo().suffix() == "png") {
            Tile *tile = new Tile(new QImage(it.filePath()));

            push_back_unique(imagesNew, tile);
        }
    }
}

void MainWindow::appendUnique(QList<Tile *> &to, QList<Tile *> &from) {
    for (Tile *image : qAsConst(from)) push_back_unique(to, image);
}

void MainWindow::push_back_unique(QList<Tile *> &toList, Tile *imageToAdd) {
    for (Tile *existingImage : qAsConst(imagesExisting)) {
        if (areIdenticalMirrored(existingImage, imageToAdd) || areRotated(existingImage, imageToAdd)) {
//            delete imageToAdd;
            return;
        }
    }
    for (Tile *newImage : qAsConst(imagesNew)) {
        if (areIdenticalMirrored(newImage, imageToAdd) || areRotated(newImage, imageToAdd)) {
//            delete imageToAdd;
            return;
        }
    }

    toList.push_back(imageToAdd);
}


void MainWindow::generateFireTiles() {
    QList<Tile *> tempImages;
    QList<Tile *> tempImagesColors;

    QList<Tile *> currImages = imagesNew;
    imagesNew.clear();

    for (Tile *tile : qAsConst(currImages)) {

        if (isRockMolten(tile)) {
            Tile *temp = createRockNormalImage(tile);
            delete tile;
            tile = temp;
        }
        if (isRockNormal(tile)) {
            QList<Tile *> createdImages = createRockMoltenImages(tile);
            if (createdImages.size() == 2) {
                tempImagesColors.append(createdImages);
            }
            else {
                tempImages.append(createdImages);
            }
        }
        else if (isUnAffectedTile(tile)) {
            tile->level = Levels::UNAFFECTED;
            tempImages.append(tile);
        }
        else {
            QList<Tile *> createdImages;

            for (int paletteIndex = 0; paletteIndex < materialColorPalettes.size(); paletteIndex++) {
                auto palette = materialColorPalettes.at(paletteIndex);
                auto asserts = allAsserts.at(paletteIndex);

                QVector<QColor> materialColors = palette.first;
                QVector<QVector<QColor>> materialFireLevelColors = palette.second;

                if (isCurrentMaterial(tile, materialColors, materialFireLevelColors)) {

                    tile->level = getMaterialIndex(tile, materialColors, materialFireLevelColors, asserts);
                    if (tile->level < Levels::ZERO) {
                        tile->level = Levels::NORMAL;
                        break;
                    }
                    else if (tile->level > Levels::EIGHT) {
                        break;
                    }

                    createdImages = createMaterialFire(tile, materialColors, materialFireLevelColors, asserts);
                    tempImages.append(createdImages);

                    break;
                }
            }

            if (createdImages.isEmpty()) {
                tile->level = getImageFireIndex(tile);
                tempImages.push_back(tile);
            }
        }
    }

    tempImages.append(tempImagesColors);
    imagesNew = tempImages;
}

void MainWindow::generateUnaffectedTiles() {
    QList<Tile *> tiles;

    for (Tile *tile : imagesNew) {

        if (isAffectedTile(tile)) {
            Tile *unaffectedTile = createUnaffectedImage(tile);

            tiles.push_back(unaffectedTile);
        }
    }

    imagesNew.append(tiles);
}

void MainWindow::dumpImages(QList<QImage *> &images) {
    int numb = 1;
    for (QImage *image : images) {
        image->save(folderPath + "/dump/" + QString::number(numb++) + ".png");
    }
}


QList<Tile *> MainWindow::createRockMoltenImages(Tile *normalTile) {
    QList<Tile *> tempImage;

    tempImage.push_back(normalTile);
    normalTile->level = getRockHasFireLevels(normalTile) ? Levels::ZERO : Levels::ROCK_NORMAL;

    int maxRockMoltenLevels = normalTile->level == Levels::ZERO ? rockMoltenLevelColors.size() : 1;

    for (int level = 0; level < maxRockMoltenLevels; level++) {
        Tile *rockMoltenTile = new Tile(new QImage(normalTile->width(), normalTile->height(), QImage::Format_ARGB32));

        if (normalTile->level == Levels::ROCK_NORMAL) {
            rockMoltenTile->level = Levels::ROCK_MOLTEN;
        }
        else {
            rockMoltenTile->level = level + 1;
        }

        for (int x = 0; x < normalTile->width(); x++) {
            for (int y = 0; y < normalTile->height(); y++) {
                QColor tileColor = normalTile->pixelColor(x, y);

                int index = rockNormalColors.indexOf(tileColor);
                if (index != -1) {
                    rockMoltenTile->setPixelColor(x, y, rockMoltenColors.at(index));
                    continue;
                }

                rockMoltenTile->setPixelColor(x, y, rockMoltenLevelColors.at(level));
            }
        }

        tempImage.push_front(rockMoltenTile);
    }

    return tempImage;
}

Tile *MainWindow::createRockNormalImage(Tile *moltenTile) {
    Tile *rockNormal = new Tile(new QImage(moltenTile->width(), moltenTile->height(), QImage::Format_ARGB32));

    for (int x = 0; x < moltenTile->width(); x++) {
        for (int y = 0; y < moltenTile->height(); y++) {
            QColor color = moltenTile->pixelColor(x, y);

            int index = rockMoltenColors.indexOf(color);
            if (index != -1) {
                rockNormal->setPixelColor(x, y, rockNormalColors.at(index));
                continue;
            }

            rockNormal->setPixelColor(x, y, rockMoltenLevelColors.first());
        }
    }

    rockNormal->level = Levels::ZERO;

    return rockNormal;
}

void MainWindow::removeDuplicates(QList<Tile *> &list) {

    QList<Tile *> tempList = list;
    list.clear();

    for (Tile *tile : tempList) {
        bool exists = false;

        for (Tile *existing : list) {
            if (tile->level != existing->level) continue;

            if (areIdentical(tile, existing)) {
                exists = true;
                break;
            }
        }
        if (!exists) {
            list.push_back(tile);
        }
    }
}

QList<Tile *> MainWindow::createMaterialFire(Tile *currentTile, QVector<QColor> &materialColors, QVector<QVector<QColor>> &materialColorLevels, QVector<int> &asserts) {

    if (asserts.at(0) != -1 && asserts.contains(currentTile->level)) {
        assert(false); // that level is not supported!
        return QList<Tile *>();
    }

    QList<Tile *> tempImages;
    for (int currentColorLevel = 0; currentColorLevel < materialColorLevels.first().size(); ++currentColorLevel) {
        if (currentTile->level == currentColorLevel) { tempImages.push_front(currentTile); continue; }

        Tile *tempImage = new Tile(new QImage(currentTile->width(), currentTile->height(), QImage::Format_ARGB32));
        tempImage->level = currentColorLevel;

        for (int x = 0; x < currentTile->width(); x++) {
            for (int y = 0; y < currentTile->height(); y++) {

                QColor tileColor = currentTile->pixelColor(x, y);

                if (materialColors.contains(tileColor)) {
                    tempImage->setPixelColor(x, y, tileColor);
                    continue;
                }

                // find color palette
                for (int paletteIndex = 0; paletteIndex < materialColorLevels.size(); paletteIndex++) {
                    QVector<QColor> colorPalette = materialColorLevels.at(paletteIndex);
                    //QVector<QColor> &colorPalette : materialColorLevels
                    if (colorPalette.at(currentTile->level) == tileColor) {
                        tempImage->setPixelColor(x, y, colorPalette.at(currentColorLevel));
                    }
                }
            }
        }

        tempImages.push_front(tempImage);

    }

    return tempImages;
}

void MainWindow::addRotatedImages() {
    QList<Tile *> tempList;

    for (Tile *tile : qAsConst(imagesNew)) {
        tempList.push_back(tile);
        tempList.push_back(createRotatedTile(tile));
    }

    imagesNew = tempList;
}

void MainWindow::addFlippedImages(bool horizontally, bool vertically) {
    QList<Tile *> tempImages;
    Tile *tempImage = nullptr;

    for (Tile *imageToFlip : qAsConst(imagesNew)) {
        tempImages.push_back(imageToFlip);

        if (horizontally) {
            tempImage = new Tile(new QImage(imageToFlip->image->mirrored(true, false)));
            tempImage->level = imageToFlip->level;
            tempImages.push_back(tempImage);
        }

        if (vertically) {
            tempImage = new Tile(new QImage(imageToFlip->image->mirrored(false, true)));
            tempImage->level = imageToFlip->level;
            tempImages.push_back(tempImage);
        }

        if (horizontally && vertically) {
            tempImage = new Tile(new QImage(imageToFlip->image->mirrored(true, true)));
            tempImage->level = imageToFlip->level;
            tempImages.push_back(tempImage);
        }


    }

    imagesNew = tempImages;

//    append(imagesNew, tempImages);
}

int MainWindow::getAverageSpectrum(Tile *image) {
    int red = 0, green = 0, blue = 0, pixels = 0;

    for (int x = 0; x < image->width(); ++x) {
        for (int y = 0; y < image->height(); ++y) {
            QColor pixel = image->pixelColor(x, y);
            if (pixel.alpha() == 0) continue;

            red += pixel.red();
            green += pixel.green();
            blue += pixel.blue();

            pixels += 1;
        }
    }
    red /= pixels;
    green /= pixels;
    blue /= pixels;

    return QColor(red, green, blue).hue();
}

void MainWindow::sortImages(QList<Tile *> &images) {    
    for (int i = 0; i < images.size() - 1; i++) {

        Tile *first = images.at(i);
        for (int j = i + 1; j < images.size(); ++j) {
            Tile *second = images.at(j);
            if (isSameAs(first, second)) {
                images.swapItemsAt(i, j);
                j = images.size();
                break;
            }
        }
    }
}

void MainWindow::sortImagesBySpectrum(QList<Tile *> &imagesNew) {
    std::sort(imagesNew.begin(), imagesNew.end(), [=](Tile *a, Tile *b) -> bool
    {
        return getAverageSpectrum(a) > getAverageSpectrum(b);
    });
}

void MainWindow::groupTransformations() {
    for (int i = 0; i < imagesNew.size() - 1; i++) {

        int mirroredFound = 0;
        Tile *first = imagesNew.at(i);
        for (int j = i + 2; j < imagesNew.size(); ++j) {
            Tile *second = imagesNew.at(j);

            if (areIdenticalMirrored(first, second)) {
                mirroredFound++;

                imagesNew.swapItemsAt(j, i + mirroredFound);
                continue;
            }

            Tile *rotatedImage = createRotatedTile(second);

            if (areIdenticalMirrored(first, rotatedImage)) {
                mirroredFound++;

                imagesNew.swapItemsAt(j, i + mirroredFound);
            }
            delete rotatedImage;
        }
    }
}

void MainWindow::groupSimilar(QList<Tile *> &images) {

    for (int i = 0; i < images.size() - 1; i++) {
        Tile *img = images.at(i);

        int lowest = INT_MAX;
        int lowestJ = i + 1;
        for (int j = i + 1; j < images.size(); j++) {
            Tile *other = images.at(j);

            int currentDiff = pixelDifference(img, other);
            if (currentDiff < lowest) {
                lowest = currentDiff;
                lowestJ = j;
            }
        }
        if (lowestJ != i + 1) {
            images.swapItemsAt(i + 1, lowestJ);
        }
    }
}

int MainWindow::pixelDifference(Tile *first, Tile *second) {
    QColor firstPixelDifference = pixelDifference(first);
    QColor secondPixelDifference = pixelDifference(second);

    int diffRed = qAbs(firstPixelDifference.red() - secondPixelDifference.red());
    int diffGreen = qAbs(firstPixelDifference.green() - secondPixelDifference.green());
    int diffBlue = qAbs(firstPixelDifference.blue() - secondPixelDifference.blue());

    return diffRed + diffGreen + diffBlue;
}

QColor MainWindow::pixelDifference(Tile *image) {
    qreal red = 0, green = 0, blue = 0;
    int timesColorExists = 0;

    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            QColor color = image->pixelColor(x, y);

            if (color.alpha() != 0) {
                red += color.red();
                green += color.green();
                blue += color.blue();
                timesColorExists++;
            }
        }
    }
    red = red / timesColorExists ;
    green = green / timesColorExists;
    blue = blue / timesColorExists;

    return QColor(red, green, blue);
}

Tile *MainWindow::createRotatedTile(Tile *image) {
    Tile *rotatedImage = new Tile(new QImage(image->width(), image->height(), QImage::Format_ARGB32));
    rotatedImage->level = image->level;

    for (int x = 0; x < image->width(); x++) {

        for (int y = 0; y < image->height(); y++) {
            QColor pixel = image->pixelColor(x, y);
            rotatedImage->setPixelColor(y, x, pixel);
        }
    }

    return rotatedImage;
}

int MainWindow::getImageFireIndex(Tile *image) {

    if (isRockNormal(image)) return getRockHasFireLevels(image);
    else if (isRockMolten(image)) return getRockMoltenIndex(image);

    for (int index = 0; index < materialColorPalettes.size(); index++) {
        int indexAt = Levels::NORMAL;
        auto palette = materialColorPalettes.at(index);
        auto asserts = allAsserts.at(index);
        indexAt = getMaterialIndex(image, palette.first, palette.second, asserts);
        if (indexAt != Levels::NORMAL) return indexAt;
    }

    return Levels::NORMAL;
}

bool MainWindow::isCurrentMaterial(Tile *image, QVector<QColor> &materialColors, QVector<QVector<QColor> > &materialColorLevels) {
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {

            QColor imagePixel = image->pixelColor(x, y);

            if (materialColors.contains(imagePixel)) continue;

            bool colorFound = false;
            for (QVector<QColor> colors : materialColorLevels) {

                if (colors.contains(imagePixel)) {
                    colorFound = true;
                    break;
                }
            }

            if (!colorFound) return false;
        }
    }

    return true;
}

int MainWindow::getMaterialIndex(Tile *image, QVector<QColor> &materialColors, QVector<QVector<QColor> > &materialColorLevels, QVector<int> asserts) {
    QVector<int> indexFoundAt(materialColorLevels.first().size());
    int numberOfFirePixels = 0;

    for (int x = 0; x < image->width(); ++x) {
        for (int y = 0; y < image->height(); y++) {
            QColor color = image->pixelColor(x, y);
            if (materialColors.contains(color)) continue;

            numberOfFirePixels++;

            bool foundFirePixel = false;
            for (int i = 0; i < materialColorLevels.size(); ++i) {
                const QVector<QColor> colorPalette = materialColorLevels.at(i);

                for (int index = 0; index < colorPalette.size(); index++) {

                    QColor colorAt = colorPalette.at(index);
                    if (colorAt == color) {
                        indexFoundAt[index] += 1;
                        foundFirePixel = true;
                    }
                }

            }
            if (!foundFirePixel) {
                return Levels::NORMAL;
            }
        }
    }

    if (numberOfFirePixels == 0) return Levels::NORMAL;
    for (int first = 0; first < indexFoundAt.size() - 1; ++first) {
        if (indexFoundAt.at(first) == 0) continue;

        for (int second = first + 1; second < indexFoundAt.size(); ++second) {
            if (indexFoundAt.at(second) == 0 || indexFoundAt.first() != numberOfFirePixels) continue;

            if (indexFoundAt.at(first) == indexFoundAt.at(second)) {
                if (asserts.contains(first)) {
                    indexFoundAt[first] = 0;
                }
                else if (asserts.contains(second)) {
                    indexFoundAt[second] = 0;
                }
                else {
                    image->save(folderPath + "/" + "assert.png");
                    assert(false); // can't have two indexes where all match!
                }
            }
        }
    }

    return indexFoundAt.indexOf(numberOfFirePixels);
}

//bool MainWindow::isRockCeramics(QImage *image) {
//    for (int x = 0; x < image->width(); x++) {
//        for (int y = 0; y < image->height(); y++) {

//            QColor imagePixel = image->pixelColor(x, y);

//            if (rockCeramicsColors.contains(imagePixel)) continue;

//            bool colorFound = false;
//            for (QVector<QColor> colors : rockCeramicsGlowingLevelColors) {

//                if (colors.contains(imagePixel)) {
//                    colorFound = true;
//                    break;
//                }
//            }

//            if (!colorFound) return false;
//        }
//    }

//    return true;
//}

bool MainWindow::isRockNormal(Tile *image) {

    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            QColor color = image->pixelColor(x, y);

            if (rockNormalColors.contains(color)) continue;
            else if (color == rockMoltenLevelColors.first()) continue;

            return false;
        }
    }
    return true;
}

int MainWindow::getRockHasFireLevels(Tile *image) {
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            if (image->pixelColor(x, y) == rockMoltenLevelColors.first()) return true;
        }
    }

    return false;
}

bool MainWindow::isRockMolten(Tile *image) {
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            QColor color = image->pixelColor(x, y);

            if (rockMoltenColors.contains(color)) continue;
            else if (rockMoltenLevelColors.contains(color)) continue;

            return false;
        }
    }
    return true;
}

int MainWindow::getRockMoltenIndex(Tile *image) {
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            QColor color = image->pixelColor(x, y);

            if (rockMoltenColors.contains(color)) continue;

            return rockMoltenLevelColors.indexOf(color) + 1;
        }
    }

    return 0;
}

bool MainWindow::isAffectedTile(Tile *tile) {
    for (int x = 0; x < tile->width(); x++) {
        for (int y = 0; y < tile->height(); y++) {
            QColor color = tile->pixelColor(x, y);
            if (!affectedTileColors.contains(color)) {
                return false;
            }
        }
    }
    return true;
}

Tile *MainWindow::createUnaffectedImage(Tile *poweredTile) {
    Tile *unaffectedTile = new Tile(new QImage(poweredTile->width(), poweredTile->height(), QImage::Format_ARGB32));
    unaffectedTile->level = Levels::UNAFFECTED;

    for (int x = 0; x < poweredTile->width(); x++) {
        for (int y = 0; y < poweredTile->height(); y++) {

            int index = affectedTileColors.indexOf(poweredTile->pixelColor(x, y));
            QColor unaffectedColor = unaffectedTileColors.at(index);

            unaffectedTile->setPixelColor(x, y, unaffectedColor);
        }
    }

    return unaffectedTile;
}

bool MainWindow::isUnAffectedTile(Tile *tile) {
    for (int x = 0; x < tile->width(); x++) {
        for (int y = 0; y < tile->height(); y++) {
            QColor color = tile->pixelColor(x, y);
            if (!unaffectedTileColors.contains(color)) {
                return false;
            }
        }
    }
    return true;
}

void MainWindow::saveAllImages(QList<Tile *> &images, QString path) {
    QVector<int> highestNumbers(9);

    for (int level = 0; level < highestNumbers.size(); level++) {
        QStringList tileFileNames = getTileFileNamesSorted(path + "/" + QString::number(level));
        if (!tileFileNames.isEmpty()) {

            highestNumbers[level] = tileFileNames.last().split(".").first().toInt();

        }
    }

    QVector<int> highestFileNumbers;

    for (int level = Levels::FIRST; level <= Levels::LAST; level++) {
        highestFileNumbers.push_back(0);

        QString levelPath = getLevelString(level);

        QStringList tileFileNames = getTileFileNamesSorted(path + "/" + levelPath);
        if (!tileFileNames.isEmpty()) {
            int normalImageNumber = tileFileNames.last().split(".").first().toInt();
            highestFileNumbers.back() = normalImageNumber;
        }
    }

    for (Tile *image : images) {

        QString newPath = path + "/" + getLevelString(image->level);

        image->save(newPath + "/" + QString::number(++highestFileNumbers[image->level]) + ".png");
    }
}

bool MainWindow::areIdentical(Tile *first, Tile *second) {
    if (first->width() != second->width() || first->height() != second->height()) exit(1);

    for (int x = 0; x < first->width(); x++) {
        for (int y = 0; y < first->height(); y++) {
            if (first->pixelColor(x, y) != second->pixelColor(x, y)) {
                return false;
            }
        }
    }

    return true;
}

bool MainWindow::isSameAs(Tile *first, Tile *second) {
    if (first->width() != second->width() || first->height() != second->height()) exit(1);

    for (int x = 0; x < first->width(); x++) {
        for (int y = 0; y < first->height(); y++) {
            if (first->pixelColor(x, y).alpha() == 0) continue;

            if (first->pixelColor(x, y) != second->pixelColor(x, y)) {
                return false;
            }
        }
    }

    return true;
}

bool MainWindow::areIdenticalMirrored(Tile *first, Tile *second) {
    if (areIdentical(first, second)) return true;

    Tile *mirrored = nullptr;

    mirrored = new Tile(new QImage(second->image->mirrored(true, false)));
    if (areIdentical(first, mirrored)) {
        delete mirrored;
        return true;
    }
    delete mirrored;

    mirrored = new Tile(new QImage(second->image->mirrored(true, true)));
    if (areIdentical(first, mirrored)) {
        delete mirrored;
        return true;
    }
    delete mirrored;

    mirrored = new Tile(new QImage(second->image->mirrored(false, true)));
    if (areIdentical(first, mirrored)) {
        delete mirrored;
        return true;
    }
    delete mirrored;

    return false;
}

bool MainWindow::areRotated(Tile *first, Tile *second) {

    Tile *tempRotatedTile = createRotatedTile(first);

    bool mirrored = areIdenticalMirrored(tempRotatedTile, second);
    delete tempRotatedTile;

    return mirrored;
}

QStringList MainWindow::getTileFileNamesSorted(QString path) {
    QDir directory(path);
    QStringList files = directory.entryList(QStringList() << "*.png",QDir::Files);

    QCollator collator;
    collator.setNumericMode(true);

    std::sort(files.begin(), files.end(), collator);

    return files;
}


MainWindow::~MainWindow()
{
    delete ui;
}

