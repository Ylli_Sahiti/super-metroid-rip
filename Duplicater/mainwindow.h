#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QMainWindow>

#include <QtAlgorithms>

#include "tile.h"
#include "globals.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);

    void loadSelectedFolder();

    void runMaintenance();
    void runCheckIsPowered();
    void runPrintIndexOfImages();
    void runDetectColorPalette();
    void runDefaultFunctionality();

    QString getLevelString(int level);

    void initMock();
    void printAssert(QVector<QColor> &fireColors, QVector<QVector<QColor>> &fireLevels);

    void loadImages(QString &filePath);

    void appendUnique(QList<Tile *> &to, QList<Tile *> &from);
    void push_back_unique(QList<Tile *> &list, Tile *image);

    void generateFireTiles();
    void generateUnaffectedTiles();
    void dumpImages(QList<QImage *> &imagesNew);
    QList<Tile *> createRockMoltenImages(Tile *normalTile);
    Tile *createRockNormalImage(Tile *fireTile);

    void removeDuplicates(QList<Tile *> &list);

    QList<Tile *> createMaterialFire(Tile *highestColorLevelTile, QVector<QColor> &materialColors, QVector<QVector<QColor>> &materialColorLevels, QVector<int> &asserts);

    void addRotatedImages();

    void addFlippedImages(bool horizontally = true, bool vertically = true);

    int getAverageSpectrum(Tile *image);
    void sortImages(QList<Tile *> &imagesNew);
    void sortImagesBySpectrum(QList<Tile *> &imagesNew);


    void groupTransformations();
    void groupSimilar(QList<Tile *> &images);

    int pixelDifference(Tile *first, Tile *second);
    QColor pixelDifference(Tile *image);

    Tile *createRotatedTile(Tile *image);

    int getImageFireIndex(Tile *image);

    bool isCurrentMaterial(Tile *image, QVector<QColor> &materialColors, QVector<QVector<QColor>> &materialColorLevels);
    int getMaterialIndex(Tile *image, QVector<QColor> &materialColors, QVector<QVector<QColor>> &materialColorLevels, QVector<int> asserts);

    bool isRockNormal(Tile *image);
    int getRockHasFireLevels(Tile *image);

    bool isRockMolten(Tile *image);
    int getRockMoltenIndex(Tile *image);

    bool isAffectedTile(Tile *image);
    Tile *createUnaffectedImage(Tile *fireTile);

    bool isUnAffectedTile(Tile *tile);

    void saveAllImages(QList<Tile *> &imagesNew, QString folderPath);

    bool areIdentical(Tile *first, Tile *second);
    bool isSameAs(Tile *first, Tile *second);
    bool areIdenticalMirrored(Tile *first, Tile *second);
    bool areRotated(Tile *first, Tile *second);

    QStringList getTileFileNamesSorted(QString folderPath);

    ~MainWindow();
private:
    Ui::MainWindow *ui;

    QString folderPath;

    QVector<QVector<int>> allAsserts;
    QVector<QPair<QVector<QColor>, QVector<QVector<QColor>>>> materialColorPalettes;

    QVector<QColor> rockNormalColors;
    QVector<QColor> rockMoltenColors;
    QVector<QColor> rockMoltenLevelColors;

    QVector<QColor> affectedTileColors;
    QVector<QColor> unaffectedTileColors;

    QList<Tile *> imagesExisting;
    QList<Tile *> imagesNew;

    QColor transparentColorWithAlpha = QColor(0, 0, 0, 0);
};
#endif // MAINWINDOW_H
