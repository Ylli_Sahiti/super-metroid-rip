#ifndef TILE_H
#define TILE_H

#include <QImage>
#include <QString>

#include "globals.h"

class Tile {
public:
    Tile(QImage *image) {
        this->image = image;
    }

    void setImage(QImage *image) {
        this->image = image;
    }

    void operator=(QImage *image) {
        this->image = image;
    }

    bool valid(int x, int y) const { return image->valid(x, y); }
    bool valid(const QPoint &pt) const { return image->valid(pt); }

    int pixelIndex(int x, int y) const { return image->pixelIndex(x, y); }
    int pixelIndex(const QPoint &pt) const { return image->pixelIndex(pt); }

    QRgb pixel(int x, int y) const { return image->pixel(x, y); }
    QRgb pixel(const QPoint &pt) const { return image->pixel(pt); }

    void setPixel(int x, int y, uint index_or_rgb) { image->setPixel(x, y, index_or_rgb); }
    void setPixel(const QPoint &pt, uint index_or_rgb) { image->setPixel(pt, index_or_rgb); }

    QColor pixelColor(int x, int y) const { return image->pixelColor(x, y); }
    QColor pixelColor(const QPoint &pt) const { return image->pixelColor(pt); }

    void setPixelColor(int x, int y, const QColor &c) { image->setPixelColor(x, y, c); }
    void setPixelColor(const QPoint &pt, const QColor &c) { image->setPixelColor(pt, c); }

    int width() const { return image->width(); }
    int height() const { return image->height(); }

    bool save(const QString &fileName, const char *format = nullptr, int quality = -1) const { return image->save(fileName, format, quality); }
    bool save(QIODevice *device, const char *format = nullptr, int quality = -1) const { return image->save(device, format, quality); }

    bool load(QIODevice *device, const char* format) { return image->load(device, format); }
    bool load(const QString &fileName, const char *format = nullptr) { return image->load(fileName, format); }

    int level = Levels::NORMAL;
    QImage *image = nullptr;

    ~Tile() { delete image; }

};

#endif // TILE_H
