#ifndef WORLDMAP_H
#define WORLDMAP_H

#include <QGraphicsItem>
#include <QPainter>

class WorldMap : public QGraphicsItem {
public:
    WorldMap() { }

    QRectF boundingRect() const override {
        return image->rect();
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override {
        Q_UNUSED(option) Q_UNUSED(widget);

        painter->drawPixmap(image->rect(), QPixmap::fromImage(*image));
    }

    void operator=(QImage *image) {
        this->image = image;
    }

    int width() {
        return image->width();
    }
    int height() {
        return image->height();
    }

    bool save(const QString &fileName, const char *format = nullptr, int quality = -1) const {
        return image->save(fileName, format, quality);
    }
    bool save(QIODevice *device, const char *format = nullptr, int quality = -1) const {
        return image->save(device, format, quality);
    }

    inline bool valid(const QPoint &pt) const { return image->valid(pt.x(), pt.y()); }
    inline int pixelIndex(const QPoint &pt) const { return image->pixelIndex(pt.x(), pt.y());}
    inline QRgb pixel(const QPoint &pt) const { return image->pixel(pt.x(), pt.y()); }
    inline void setPixel(const QPoint &pt, uint index_or_rgb) { image->setPixel(pt.x(), pt.y(), index_or_rgb); }
    inline void setPixel(int x, int y, uint index_or_rgb) { image->setPixel(x, y, index_or_rgb); }
    inline QColor pixelColor(const QPoint &pt) const { return image->pixelColor(pt.x(), pt.y()); }
    inline QColor pixelColor(int x, int y) const { return image->pixelColor(x, y); }
    inline void setPixelColor(const QPoint &pt, const QColor &c) { image->setPixelColor(pt.x(), pt.y(), c); }
    inline void setPixelColor(int x, int y, const QColor &c) { image->setPixelColor(x, y, c); }


private:
    QImage *image;

};

#endif // WORLDMAP_H
