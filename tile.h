#ifndef TILE_H
#define TILE_H

#include <QGraphicsItem>
#include <QPainter>
#include <QDebug>

#include "globals.h"

class Tile : public QGraphicsItem {
public:
    Tile(TileType type, QImage *image, const QPoint &scenePos);

    enum { Type = QGraphicsItem::UserType };
    int type() const override { return Type; }

private:
    static QImage *setHighlightedImage();
    static QImage *setOutOfBoundsImageImage();
    static QImage *setBackgroundImage();

public:
    static void makeBlack(bool value) { showAsBlack = value; }
    static void setTypesToShow(TileType types) { typesToShow = types; }
    bool isOutOfBounds() { return tileType == TileType::none; }

    void setImage(QImage *image) { this->image = image; }

    bool isBackground() { return isBackgroundTile; }
    void makeBackground();

    QRectF boundingRect() const override { return QRect(QPoint(0, 0), QPoint(15, 15)); }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

    inline bool valid(const QPoint &pt) const { return image->valid(pt.x(), pt.y()); }
    inline int pixelIndex(const QPoint &pt) const { return image->pixelIndex(pt.x(), pt.y());}
    inline QRgb pixel(const QPoint &pt) const { return image->pixel(pt.x(), pt.y()); }
    inline void setPixel(const QPoint &pt, uint index_or_rgb) { image->setPixel(pt.x(), pt.y(), index_or_rgb); }
    inline void setPixel(int x, int y, uint index_or_rgb) { image->setPixel(x, y, index_or_rgb); }
    inline QColor pixelColor(const QPoint &pt) const { return image->pixelColor(pt.x(), pt.y()); }
    inline QColor pixelColor(int x, int y) const { return image->pixelColor(x, y); }
    inline void setPixelColor(const QPoint &pt, const QColor &c) { image->setPixelColor(pt.x(), pt.y(), c); }
    inline void setPixelColor(int x, int y, const QColor &c) { image->setPixelColor(x, y, c); }

    inline int width() const { return image->width(); }
    inline int height() const { return  image->height(); }

private:

    bool isBackgroundTile = false;

    TileType tileType = TileType::none;

    QImage *image;

    static TileType typesToShow;
    static bool showAsBlack;
    static QImage *highlightedImage;
    static QImage *outOfBoundsImage;
    static QImage *backgroundImage;

    // QGraphicsItem interface
};

#endif // TILE_H
