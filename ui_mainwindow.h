/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_16;
    QSplitter *splitter;
    QGraphicsView *graphicsView;
    QTabWidget *TabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_13;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_6;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *leftboundLineEdit;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QLineEdit *topboundLineEdit;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_6;
    QLineEdit *rightboundLineEdit;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_4;
    QLineEdit *bottomboundLineEdit;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnMakeImageOfMap;
    QHBoxLayout *horizontalLayout_5;
    QCheckBox *worldmapCheckBox;
    QCheckBox *missingCheckBox;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *btnRipLeft;
    QPushButton *btnRipTiles;
    QPushButton *btnRipRight;
    QGroupBox *groupBox_8;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_13;
    QPushButton *btnMatchScreen;
    QPushButton *btnMatchTilesToMap;
    QGroupBox *filterGroupBox;
    QVBoxLayout *verticalLayout_17;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_9;
    QCheckBox *zoneNoneCheckBox;
    QCheckBox *zoneGeneralCheckBox;
    QCheckBox *zoneSpaceColonyCheckBox;
    QHBoxLayout *horizontalLayout_8;
    QCheckBox *zoneCrateriaCheckBox;
    QCheckBox *zoneBrinstarCheckBox;
    QCheckBox *zoneNorfairCheckBox;
    QHBoxLayout *horizontalLayout_6;
    QCheckBox *zoneWreckedShipCheckBox;
    QCheckBox *zoneMaridiaCheckBox;
    QCheckBox *zoneTourianCheckBox;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_12;
    QHBoxLayout *horizontalLayout_16;
    QCheckBox *miscNoneCheckBox;
    QCheckBox *miscTestTileCheckBox;
    QHBoxLayout *horizontalLayout_12;
    QCheckBox *miscPlayerCheckBox;
    QCheckBox *miscStatuesCheckBox;
    QHBoxLayout *horizontalLayout_15;
    QCheckBox *miscDestructablesCheckBox;
    QCheckBox *miscCollectablesCheckBox;
    QHBoxLayout *horizontalLayout_14;
    QCheckBox *miscGatewaysCheckBox;
    QCheckBox *miscElevatorsCheckBox;
    QHBoxLayout *horizontalLayout_2;
    QCheckBox *miscRoomMapCheckBox;
    QCheckBox *miscRoomSaveCheckBox;
    QCheckBox *miscRoomSupplyCheckBox;
    QSpacerItem *verticalSpacer_2;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_15;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_2;
    QVBoxLayout *verticalLayout_14;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_9;
    QVBoxLayout *verticalLayout_8;
    QPushButton *btnLoadIcon;
    QPushButton *btnDeleteIcon;
    QPushButton *btnSaveWorldmapChanges;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_7;
    QPushButton *btnSaveMatrix;
    QHBoxLayout *horizontalLayout_11;
    QPushButton *btnMakeBackground;
    QPushButton *btnDelete;
    QHBoxLayout *horizontalLayout_10;
    QCheckBox *makeBackgroundCheckBox;
    QCheckBox *deleteCheckBox;
    QCheckBox *targetCheckBox;
    QSpacerItem *verticalSpacer;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1039, 774);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setMinimumSize(QSize(0, 0));
        centralwidget->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_16 = new QVBoxLayout(centralwidget);
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        graphicsView = new QGraphicsView(splitter);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        splitter->addWidget(graphicsView);
        TabWidget = new QTabWidget(splitter);
        TabWidget->setObjectName(QString::fromUtf8("TabWidget"));
        TabWidget->setMinimumSize(QSize(295, 0));
        TabWidget->setMaximumSize(QSize(295, 16777215));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        tab->setMinimumSize(QSize(295, 0));
        tab->setMaximumSize(QSize(295, 16777215));
        verticalLayout_13 = new QVBoxLayout(tab);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        verticalLayout_13->setContentsMargins(0, 0, 0, 0);
        scrollArea = new QScrollArea(tab);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 298, 670));
        verticalLayout_6 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        groupBox = new QGroupBox(scrollAreaWidgetContents);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_5 = new QVBoxLayout(groupBox);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        leftboundLineEdit = new QLineEdit(groupBox);
        leftboundLineEdit->setObjectName(QString::fromUtf8("leftboundLineEdit"));

        verticalLayout->addWidget(leftboundLineEdit);


        horizontalLayout_3->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_2->addWidget(label_3);

        topboundLineEdit = new QLineEdit(groupBox);
        topboundLineEdit->setObjectName(QString::fromUtf8("topboundLineEdit"));

        verticalLayout_2->addWidget(topboundLineEdit);


        horizontalLayout_3->addLayout(verticalLayout_2);


        verticalLayout_5->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_3->addWidget(label_6);

        rightboundLineEdit = new QLineEdit(groupBox);
        rightboundLineEdit->setObjectName(QString::fromUtf8("rightboundLineEdit"));

        verticalLayout_3->addWidget(rightboundLineEdit);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_4->addWidget(label_4);

        bottomboundLineEdit = new QLineEdit(groupBox);
        bottomboundLineEdit->setObjectName(QString::fromUtf8("bottomboundLineEdit"));

        verticalLayout_4->addWidget(bottomboundLineEdit);


        horizontalLayout->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        btnMakeImageOfMap = new QPushButton(groupBox);
        btnMakeImageOfMap->setObjectName(QString::fromUtf8("btnMakeImageOfMap"));

        horizontalLayout_7->addWidget(btnMakeImageOfMap);


        verticalLayout_5->addLayout(horizontalLayout_7);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        worldmapCheckBox = new QCheckBox(groupBox);
        worldmapCheckBox->setObjectName(QString::fromUtf8("worldmapCheckBox"));
        worldmapCheckBox->setChecked(true);

        horizontalLayout_5->addWidget(worldmapCheckBox);

        missingCheckBox = new QCheckBox(groupBox);
        missingCheckBox->setObjectName(QString::fromUtf8("missingCheckBox"));

        horizontalLayout_5->addWidget(missingCheckBox);


        verticalLayout_5->addLayout(horizontalLayout_5);


        verticalLayout_6->addWidget(groupBox);

        groupBox_3 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_3);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        btnRipLeft = new QPushButton(groupBox_3);
        btnRipLeft->setObjectName(QString::fromUtf8("btnRipLeft"));

        horizontalLayout_4->addWidget(btnRipLeft);

        btnRipTiles = new QPushButton(groupBox_3);
        btnRipTiles->setObjectName(QString::fromUtf8("btnRipTiles"));

        horizontalLayout_4->addWidget(btnRipTiles);

        btnRipRight = new QPushButton(groupBox_3);
        btnRipRight->setObjectName(QString::fromUtf8("btnRipRight"));

        horizontalLayout_4->addWidget(btnRipRight);


        verticalLayout_6->addWidget(groupBox_3);

        groupBox_8 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        verticalLayout_11 = new QVBoxLayout(groupBox_8);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        btnMatchScreen = new QPushButton(groupBox_8);
        btnMatchScreen->setObjectName(QString::fromUtf8("btnMatchScreen"));

        horizontalLayout_13->addWidget(btnMatchScreen);

        btnMatchTilesToMap = new QPushButton(groupBox_8);
        btnMatchTilesToMap->setObjectName(QString::fromUtf8("btnMatchTilesToMap"));

        horizontalLayout_13->addWidget(btnMatchTilesToMap);


        verticalLayout_11->addLayout(horizontalLayout_13);


        verticalLayout_6->addWidget(groupBox_8);

        filterGroupBox = new QGroupBox(scrollAreaWidgetContents);
        filterGroupBox->setObjectName(QString::fromUtf8("filterGroupBox"));
        verticalLayout_17 = new QVBoxLayout(filterGroupBox);
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        groupBox_7 = new QGroupBox(filterGroupBox);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        verticalLayout_10 = new QVBoxLayout(groupBox_7);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        zoneNoneCheckBox = new QCheckBox(groupBox_7);
        zoneNoneCheckBox->setObjectName(QString::fromUtf8("zoneNoneCheckBox"));
        zoneNoneCheckBox->setChecked(true);

        horizontalLayout_9->addWidget(zoneNoneCheckBox);

        zoneGeneralCheckBox = new QCheckBox(groupBox_7);
        zoneGeneralCheckBox->setObjectName(QString::fromUtf8("zoneGeneralCheckBox"));

        horizontalLayout_9->addWidget(zoneGeneralCheckBox);

        zoneSpaceColonyCheckBox = new QCheckBox(groupBox_7);
        zoneSpaceColonyCheckBox->setObjectName(QString::fromUtf8("zoneSpaceColonyCheckBox"));
        zoneSpaceColonyCheckBox->setChecked(false);

        horizontalLayout_9->addWidget(zoneSpaceColonyCheckBox);


        verticalLayout_10->addLayout(horizontalLayout_9);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        zoneCrateriaCheckBox = new QCheckBox(groupBox_7);
        zoneCrateriaCheckBox->setObjectName(QString::fromUtf8("zoneCrateriaCheckBox"));
        zoneCrateriaCheckBox->setChecked(false);

        horizontalLayout_8->addWidget(zoneCrateriaCheckBox);

        zoneBrinstarCheckBox = new QCheckBox(groupBox_7);
        zoneBrinstarCheckBox->setObjectName(QString::fromUtf8("zoneBrinstarCheckBox"));
        zoneBrinstarCheckBox->setChecked(false);

        horizontalLayout_8->addWidget(zoneBrinstarCheckBox);

        zoneNorfairCheckBox = new QCheckBox(groupBox_7);
        zoneNorfairCheckBox->setObjectName(QString::fromUtf8("zoneNorfairCheckBox"));
        zoneNorfairCheckBox->setChecked(false);

        horizontalLayout_8->addWidget(zoneNorfairCheckBox);


        verticalLayout_10->addLayout(horizontalLayout_8);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        zoneWreckedShipCheckBox = new QCheckBox(groupBox_7);
        zoneWreckedShipCheckBox->setObjectName(QString::fromUtf8("zoneWreckedShipCheckBox"));
        zoneWreckedShipCheckBox->setChecked(false);

        horizontalLayout_6->addWidget(zoneWreckedShipCheckBox);

        zoneMaridiaCheckBox = new QCheckBox(groupBox_7);
        zoneMaridiaCheckBox->setObjectName(QString::fromUtf8("zoneMaridiaCheckBox"));
        zoneMaridiaCheckBox->setChecked(false);

        horizontalLayout_6->addWidget(zoneMaridiaCheckBox);

        zoneTourianCheckBox = new QCheckBox(groupBox_7);
        zoneTourianCheckBox->setObjectName(QString::fromUtf8("zoneTourianCheckBox"));
        zoneTourianCheckBox->setChecked(false);

        horizontalLayout_6->addWidget(zoneTourianCheckBox);


        verticalLayout_10->addLayout(horizontalLayout_6);


        verticalLayout_17->addWidget(groupBox_7);

        groupBox_6 = new QGroupBox(filterGroupBox);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        verticalLayout_12 = new QVBoxLayout(groupBox_6);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        miscNoneCheckBox = new QCheckBox(groupBox_6);
        miscNoneCheckBox->setObjectName(QString::fromUtf8("miscNoneCheckBox"));
        miscNoneCheckBox->setChecked(false);

        horizontalLayout_16->addWidget(miscNoneCheckBox);

        miscTestTileCheckBox = new QCheckBox(groupBox_6);
        miscTestTileCheckBox->setObjectName(QString::fromUtf8("miscTestTileCheckBox"));

        horizontalLayout_16->addWidget(miscTestTileCheckBox);


        verticalLayout_12->addLayout(horizontalLayout_16);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        miscPlayerCheckBox = new QCheckBox(groupBox_6);
        miscPlayerCheckBox->setObjectName(QString::fromUtf8("miscPlayerCheckBox"));

        horizontalLayout_12->addWidget(miscPlayerCheckBox);

        miscStatuesCheckBox = new QCheckBox(groupBox_6);
        miscStatuesCheckBox->setObjectName(QString::fromUtf8("miscStatuesCheckBox"));

        horizontalLayout_12->addWidget(miscStatuesCheckBox);


        verticalLayout_12->addLayout(horizontalLayout_12);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        miscDestructablesCheckBox = new QCheckBox(groupBox_6);
        miscDestructablesCheckBox->setObjectName(QString::fromUtf8("miscDestructablesCheckBox"));

        horizontalLayout_15->addWidget(miscDestructablesCheckBox);

        miscCollectablesCheckBox = new QCheckBox(groupBox_6);
        miscCollectablesCheckBox->setObjectName(QString::fromUtf8("miscCollectablesCheckBox"));

        horizontalLayout_15->addWidget(miscCollectablesCheckBox);


        verticalLayout_12->addLayout(horizontalLayout_15);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        miscGatewaysCheckBox = new QCheckBox(groupBox_6);
        miscGatewaysCheckBox->setObjectName(QString::fromUtf8("miscGatewaysCheckBox"));

        horizontalLayout_14->addWidget(miscGatewaysCheckBox);

        miscElevatorsCheckBox = new QCheckBox(groupBox_6);
        miscElevatorsCheckBox->setObjectName(QString::fromUtf8("miscElevatorsCheckBox"));

        horizontalLayout_14->addWidget(miscElevatorsCheckBox);


        verticalLayout_12->addLayout(horizontalLayout_14);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        miscRoomMapCheckBox = new QCheckBox(groupBox_6);
        miscRoomMapCheckBox->setObjectName(QString::fromUtf8("miscRoomMapCheckBox"));

        horizontalLayout_2->addWidget(miscRoomMapCheckBox);

        miscRoomSaveCheckBox = new QCheckBox(groupBox_6);
        miscRoomSaveCheckBox->setObjectName(QString::fromUtf8("miscRoomSaveCheckBox"));

        horizontalLayout_2->addWidget(miscRoomSaveCheckBox);

        miscRoomSupplyCheckBox = new QCheckBox(groupBox_6);
        miscRoomSupplyCheckBox->setObjectName(QString::fromUtf8("miscRoomSupplyCheckBox"));

        horizontalLayout_2->addWidget(miscRoomSupplyCheckBox);


        verticalLayout_12->addLayout(horizontalLayout_2);


        verticalLayout_17->addWidget(groupBox_6);


        verticalLayout_6->addWidget(filterGroupBox);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_2);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_13->addWidget(scrollArea);

        TabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_15 = new QVBoxLayout(tab_2);
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        verticalLayout_15->setContentsMargins(0, 0, 0, 0);
        scrollArea_2 = new QScrollArea(tab_2);
        scrollArea_2->setObjectName(QString::fromUtf8("scrollArea_2"));
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 287, 687));
        verticalLayout_14 = new QVBoxLayout(scrollAreaWidgetContents_2);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        groupBox_5 = new QGroupBox(scrollAreaWidgetContents_2);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        verticalLayout_9 = new QVBoxLayout(groupBox_5);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        btnLoadIcon = new QPushButton(groupBox_5);
        btnLoadIcon->setObjectName(QString::fromUtf8("btnLoadIcon"));

        verticalLayout_8->addWidget(btnLoadIcon);

        btnDeleteIcon = new QPushButton(groupBox_5);
        btnDeleteIcon->setObjectName(QString::fromUtf8("btnDeleteIcon"));

        verticalLayout_8->addWidget(btnDeleteIcon);


        verticalLayout_9->addLayout(verticalLayout_8);

        btnSaveWorldmapChanges = new QPushButton(groupBox_5);
        btnSaveWorldmapChanges->setObjectName(QString::fromUtf8("btnSaveWorldmapChanges"));

        verticalLayout_9->addWidget(btnSaveWorldmapChanges);


        verticalLayout_14->addWidget(groupBox_5);

        groupBox_4 = new QGroupBox(scrollAreaWidgetContents_2);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        verticalLayout_7 = new QVBoxLayout(groupBox_4);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        btnSaveMatrix = new QPushButton(groupBox_4);
        btnSaveMatrix->setObjectName(QString::fromUtf8("btnSaveMatrix"));

        verticalLayout_7->addWidget(btnSaveMatrix);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        btnMakeBackground = new QPushButton(groupBox_4);
        btnMakeBackground->setObjectName(QString::fromUtf8("btnMakeBackground"));

        horizontalLayout_11->addWidget(btnMakeBackground);

        btnDelete = new QPushButton(groupBox_4);
        btnDelete->setObjectName(QString::fromUtf8("btnDelete"));

        horizontalLayout_11->addWidget(btnDelete);


        verticalLayout_7->addLayout(horizontalLayout_11);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        makeBackgroundCheckBox = new QCheckBox(groupBox_4);
        makeBackgroundCheckBox->setObjectName(QString::fromUtf8("makeBackgroundCheckBox"));

        horizontalLayout_10->addWidget(makeBackgroundCheckBox);

        deleteCheckBox = new QCheckBox(groupBox_4);
        deleteCheckBox->setObjectName(QString::fromUtf8("deleteCheckBox"));

        horizontalLayout_10->addWidget(deleteCheckBox);

        targetCheckBox = new QCheckBox(groupBox_4);
        targetCheckBox->setObjectName(QString::fromUtf8("targetCheckBox"));

        horizontalLayout_10->addWidget(targetCheckBox);


        verticalLayout_7->addLayout(horizontalLayout_10);


        verticalLayout_14->addWidget(groupBox_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer);

        scrollArea_2->setWidget(scrollAreaWidgetContents_2);

        verticalLayout_15->addWidget(scrollArea_2);

        TabWidget->addTab(tab_2, QString());
        splitter->addWidget(TabWidget);

        verticalLayout_16->addWidget(splitter);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1039, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        TabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "GroupBox", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Left", nullptr));
        leftboundLineEdit->setText(QCoreApplication::translate("MainWindow", "73", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Top", nullptr));
        topboundLineEdit->setText(QCoreApplication::translate("MainWindow", "6562", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "Right", nullptr));
        rightboundLineEdit->setText(QCoreApplication::translate("MainWindow", "73", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "Bottom:", nullptr));
        bottomboundLineEdit->setText(QCoreApplication::translate("MainWindow", "6562", nullptr));
        btnMakeImageOfMap->setText(QCoreApplication::translate("MainWindow", "Make image", nullptr));
        worldmapCheckBox->setText(QCoreApplication::translate("MainWindow", "Worldmap", nullptr));
        missingCheckBox->setText(QCoreApplication::translate("MainWindow", "Missing", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "Rip tiles", nullptr));
        btnRipLeft->setText(QCoreApplication::translate("MainWindow", "Left", nullptr));
        btnRipTiles->setText(QCoreApplication::translate("MainWindow", "All", nullptr));
        btnRipRight->setText(QCoreApplication::translate("MainWindow", "Right", nullptr));
        groupBox_8->setTitle(QCoreApplication::translate("MainWindow", "GroupBox", nullptr));
        btnMatchScreen->setText(QCoreApplication::translate("MainWindow", "Match Screen", nullptr));
        btnMatchTilesToMap->setText(QCoreApplication::translate("MainWindow", "Match Dimentions", nullptr));
        filterGroupBox->setTitle(QCoreApplication::translate("MainWindow", "Tile Types", nullptr));
        groupBox_7->setTitle(QCoreApplication::translate("MainWindow", "Zone", nullptr));
        zoneNoneCheckBox->setText(QCoreApplication::translate("MainWindow", "None", nullptr));
        zoneGeneralCheckBox->setText(QCoreApplication::translate("MainWindow", "General", nullptr));
        zoneSpaceColonyCheckBox->setText(QCoreApplication::translate("MainWindow", "Space Colony", nullptr));
        zoneCrateriaCheckBox->setText(QCoreApplication::translate("MainWindow", "Crateria", nullptr));
        zoneBrinstarCheckBox->setText(QCoreApplication::translate("MainWindow", "Brinstar", nullptr));
        zoneNorfairCheckBox->setText(QCoreApplication::translate("MainWindow", "Norfair", nullptr));
        zoneWreckedShipCheckBox->setText(QCoreApplication::translate("MainWindow", "Wrecked Ship", nullptr));
        zoneMaridiaCheckBox->setText(QCoreApplication::translate("MainWindow", "Maridia", nullptr));
        zoneTourianCheckBox->setText(QCoreApplication::translate("MainWindow", "Tourian", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("MainWindow", "Misc", nullptr));
        miscNoneCheckBox->setText(QCoreApplication::translate("MainWindow", "None", nullptr));
        miscTestTileCheckBox->setText(QCoreApplication::translate("MainWindow", "Test Tiles", nullptr));
        miscPlayerCheckBox->setText(QCoreApplication::translate("MainWindow", "Player", nullptr));
        miscStatuesCheckBox->setText(QCoreApplication::translate("MainWindow", "Statues", nullptr));
        miscDestructablesCheckBox->setText(QCoreApplication::translate("MainWindow", "Destructables", nullptr));
        miscCollectablesCheckBox->setText(QCoreApplication::translate("MainWindow", "Collectables", nullptr));
        miscGatewaysCheckBox->setText(QCoreApplication::translate("MainWindow", "Gateways", nullptr));
        miscElevatorsCheckBox->setText(QCoreApplication::translate("MainWindow", "Elevators", nullptr));
        miscRoomMapCheckBox->setText(QCoreApplication::translate("MainWindow", "Map room", nullptr));
        miscRoomSaveCheckBox->setText(QCoreApplication::translate("MainWindow", "Save room", nullptr));
        miscRoomSupplyCheckBox->setText(QCoreApplication::translate("MainWindow", "Supply room", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "Tab 1", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("MainWindow", "Worldmap big image", nullptr));
        btnLoadIcon->setText(QCoreApplication::translate("MainWindow", "Load image", nullptr));
        btnDeleteIcon->setText(QCoreApplication::translate("MainWindow", "Delete", nullptr));
        btnSaveWorldmapChanges->setText(QCoreApplication::translate("MainWindow", "Save Worldmap changes", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "Worldmap Matrix", nullptr));
        btnSaveMatrix->setText(QCoreApplication::translate("MainWindow", "Save map matrix", nullptr));
        btnMakeBackground->setText(QCoreApplication::translate("MainWindow", "make background", nullptr));
        btnDelete->setText(QCoreApplication::translate("MainWindow", "delete", nullptr));
        makeBackgroundCheckBox->setText(QCoreApplication::translate("MainWindow", "Make background", nullptr));
        deleteCheckBox->setText(QCoreApplication::translate("MainWindow", "Delete", nullptr));
        targetCheckBox->setText(QCoreApplication::translate("MainWindow", "Target", nullptr));
        TabWidget->setTabText(TabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "Tab 2", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
