#ifndef PRODUCER_H
#define PRODUCER_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include <QPoint>
#include <QPair>

#include <QList>

#include "tile.h"
#include "worldmap.h"

class Producer : public QThread {
    Q_OBJECT

public:
    Producer(QMutex &mutex, QList<Tile *> &tiles, QMap<TileType, QVector<QImage *> > &zoneIDMap, WorldMap *worldmap, QObject *parent = NULL)
        : QThread(parent), zoneIdImageMap(zoneIDMap), globalWorldmap(worldmap), globalTiles(tiles), globalMutex(mutex) {
        Producer::increaseThreadCount();
    }
    ~Producer() {
        Producer::decreaseThreadCount();
    }

    void run() override {
        QList<Tile *> tempTiles;
        int maxTiles = 10;

        for (int yOffset = globalArea.first.y(); yOffset < globalArea.second.y(); yOffset += 16) {
            for (int xOffset = globalArea.first.x(); xOffset < globalArea.second.x(); xOffset += 16) {

                Tile *tile = nullptr;

                QPoint scenePos(xOffset, yOffset);

                if (!isOutOfBoundsTile(scenePos)) {
                    for (TileType type = TileType::first; type <= TileType::last; type = TileType((int)type * 2)) {
                        if (!((int)type & (int)activeTypes)) continue;

                        for (QImage *image : zoneIdImageMap[type]) {

                            if (areSame(image, scenePos)) {
                                tile = new Tile(type, image, scenePos);
                                tempTiles.push_back(tile);
                            }
                        }

                        if (tile) break;
                    }
                }

                if (tempTiles.size() < maxTiles) continue;

                globalMutex.lock();
                globalTiles.append(tempTiles);
                globalMutex.unlock();
                tempTiles.clear();

                emit newItemAdded();

            }
        }

        if (!tempTiles.isEmpty()) {
            globalMutex.lock();
            globalTiles.append(tempTiles);
            globalMutex.unlock();
        }

        threadFinished(this);
    }

    void setupThread(QPair<QPoint, QPoint> globalArea, TileType activeTypes) {
        this->globalArea = globalArea;
        this->activeTypes = activeTypes;
    }

    static int getThreadCount() {
        return threadCount;
    }
    static int getThreadCountMax() {
        return threadCountMax;
    }



signals:
    void newItemAdded();
    void threadFinished(Producer *self);

private:

    static void increaseThreadCount() {
        Producer::threadCountMutex.lock();
        Producer::threadCount += 1;
        Producer::threadCountMutex.unlock();
    }
    static void decreaseThreadCount() {
        Producer::threadCountMutex.lock();
        Producer::threadCount -= 1;
        Producer::threadCountMutex.unlock();
    }

    bool isOutOfBoundsTile(const QPoint &scenePos) {
        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                QColor pixelColor = globalWorldmap->pixelColor(scenePos.x() + x, scenePos.y() + y);
                if (pixelColor != QColor(0, 0, 0)) {
                    return false;
                }
            }
        }
        return true;
    }

    bool areSame(QImage *image, QPoint &scenePos) {
        bool onlyTransparent = true;
        for (int y = 0; y < image->width(); y++) {
            for (int x = 0; x < image->height(); x++) {

                QColor imagePixel = image->pixelColor(x, y);
                if (imagePixel.alpha() == 0) continue;

                onlyTransparent = false;

                QColor worldmapPixel = globalWorldmap->pixelColor(scenePos.x() + x, scenePos.y() + y);

                if (imagePixel == QColor(0, 8, 0) && worldmapPixel == QColor(0, 0, 0)) {
                    continue;
                }
                else if (imagePixel != worldmapPixel) return false;
            }
        }

        return  !onlyTransparent;
    }

private:

    static int threadCountMax;
    static int threadCount;
    static QMutex threadCountMutex;

    QMap<TileType, QVector<QImage *> > &zoneIdImageMap;

    WorldMap *globalWorldmap;
    TileType activeTypes;
    QPair<QPoint, QPoint> globalArea;

    QList<Tile *> &globalTiles;
    QMutex &globalMutex;
};

#endif // PRODUCER_H
