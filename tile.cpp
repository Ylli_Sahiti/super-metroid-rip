#include "tile.h"
#include "tile.h"

TileType Tile::typesToShow = TileType::none;
bool Tile::showAsBlack = false;

QImage *Tile::highlightedImage = setHighlightedImage();
QImage *Tile::outOfBoundsImage = setOutOfBoundsImageImage();
QImage *Tile::backgroundImage = setBackgroundImage();

Tile::Tile(TileType type, QImage *image, const QPoint &scenePos) {
    this->tileType = type;
    this->image = image;
    setPos(scenePos);

    if (!isOutOfBounds()) {
        setFlag(QGraphicsItem::ItemIsSelectable);
    }
}

QImage *Tile::setHighlightedImage() {
    QImage *image = new QImage(16, 16, QImage::Format_ARGB32);
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            image->setPixelColor(x, y, QColor(0, 0, 0, 128));
        }
    }

    return image;
}

QImage *Tile::setOutOfBoundsImageImage() {
    QImage *image = new QImage(16, 16, QImage::Format_ARGB32);
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            image->setPixelColor(x, y, QColor(255, 255, 255, 255));
        }
    }

    return image;
}

QImage *Tile::setBackgroundImage() {
    QImage *image = new QImage(16, 16, QImage::Format_ARGB32);
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            image->setPixelColor(x, y, QColor(0, 0, 0, 0));
        }
    }

    return image;
}

void Tile::makeBackground() {
    prepareGeometryChange();
    this->image = backgroundImage;
    this->isBackgroundTile = true;
}

void Tile::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option) Q_UNUSED(widget);

    if (isOutOfBounds()) {
        painter->drawPixmap(outOfBoundsImage->rect(), QPixmap::fromImage(*outOfBoundsImage));
    }
    else if ((int)tileType & (int)typesToShow) {
        if (isBackground()) {
            painter->drawPixmap(backgroundImage->rect(), QPixmap::fromImage(*backgroundImage));
        }
        else if (showAsBlack) {
            painter->drawPixmap(outOfBoundsImage->rect(), QPixmap::fromImage(*outOfBoundsImage));
            return;
        }
        else {
            painter->drawPixmap(image->rect(), QPixmap::fromImage(*image));
            if (isSelected()) {
                painter->drawPixmap(highlightedImage->rect(), QPixmap::fromImage(*highlightedImage));
            }
        }
    }
}
