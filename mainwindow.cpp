#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QList>
#include <QPoint>
#include <QPair>
#include <QBuffer>
#include <QCollator>
#include <QtMath>
#include <QDirIterator>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);

//    testFunction2();

    view = ui->graphicsView;
    scene = new QGraphicsScene();

    view->setScene(scene);
    view->viewport()->installEventFilter(this);

    connectZoneCheckboxes();
    connectMiscCheckboxes();

    connect(ui->btnMakeImageOfMap, &QPushButton::released, this, &MainWindow::makeImageOfMapPressed);

    connect(ui->btnDelete, &QPushButton::released, this, &MainWindow::deleteSelected);
    connect(ui->btnMakeBackground, &QPushButton::released, this, &MainWindow::makeBackground);

    connect(ui->btnRipRight, &QPushButton::released, this, &MainWindow::ripRight);
    connect(ui->btnRipTiles, &QPushButton::released, this, &MainWindow::ripTiles);
    connect(ui->btnRipLeft, &QPushButton::released, this, &MainWindow::ripLeft);
    connect(ui->btnMatchTilesToMap, &QPushButton::released, this, &MainWindow::matchTilesToWorldmap);
    connect(ui->btnMatchScreen, &QPushButton::released, this, &MainWindow::matchScreenToWorldmap);
    connect(ui->btnSaveMatrix, &QPushButton::released, this, &MainWindow::saveMatrix);
    connect(ui->worldmapCheckBox, &QCheckBox::stateChanged, this, &MainWindow::showWorldmap);
    connect(ui->missingCheckBox, &QCheckBox::stateChanged, this, &MainWindow::makeBlack);

    connect(ui->btnLoadIcon, &QPushButton::released, this, &MainWindow::btnLoadIconPressed);
    connect(ui->btnDeleteIcon, &QPushButton::released, this, &MainWindow::btnDeleteIconPressed);
    connect(ui->btnSaveWorldmapChanges, &QPushButton::released, this, &MainWindow::saveWorldmapChangesPushed);

    imageToRip = nullptr;

    loadWorldmap();
    loadCurrentSprites();
    loadWorldmapMatrix();

    view->setDragMode(QGraphicsView::DragMode::RubberBandDrag);
    calculateTickedTileTypes();

    ui->TabWidget->setMinimumWidth(295);
    ui->TabWidget->setMaximumWidth(295);
}

MainWindow::~MainWindow() {
    delete ui;
}

bool MainWindow::addUniqueImageInVector(QVector<QImage *> &images, QImage *imageToInsert) {
    for (auto *image : images) {
        if (areDuplicates(image, imageToInsert)) {
            return false;
        }
    }

    images.append(imageToInsert);
    return true;
}

void MainWindow::connectZoneCheckboxes() {
    connect(ui->zoneNoneCheckBox, &QCheckBox::released, this, &MainWindow::zoneNoneToggled);

    connect(ui->zoneGeneralCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->zoneSpaceColonyCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->zoneCrateriaCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->zoneBrinstarCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->zoneNorfairCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->zoneWreckedShipCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->zoneMaridiaCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->zoneTourianCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
}

void MainWindow::zoneNoneToggled() {
    if (ui->zoneNoneCheckBox->isChecked()) {
        ui->zoneGeneralCheckBox->setChecked(false);
        ui->zoneSpaceColonyCheckBox->setChecked(false);
        ui->zoneCrateriaCheckBox->setChecked(false);
        ui->zoneBrinstarCheckBox->setChecked(false);
        ui->zoneNorfairCheckBox->setChecked(false);
        ui->zoneWreckedShipCheckBox->setChecked(false);
        ui->zoneMaridiaCheckBox->setChecked(false);
        ui->zoneTourianCheckBox->setChecked(false);
    }
    else {
        ui->zoneGeneralCheckBox->setChecked(true);
        ui->zoneSpaceColonyCheckBox->setChecked(true);
        ui->zoneCrateriaCheckBox->setChecked(true);
        ui->zoneBrinstarCheckBox->setChecked(true);
        ui->zoneNorfairCheckBox->setChecked(true);
        ui->zoneWreckedShipCheckBox->setChecked(true);
        ui->zoneMaridiaCheckBox->setChecked(true);
        ui->zoneTourianCheckBox->setChecked(true);
    }

    calculateTickedTileTypes();
    invalidate();
}


void MainWindow::connectMiscCheckboxes() {
    connect(ui->miscNoneCheckBox, &QCheckBox::released, this, &MainWindow::miscNoneToggled);

    connect(ui->miscDestructablesCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscPlayerCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscCollectablesCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscGatewaysCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscElevatorsCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscRoomMapCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscRoomSaveCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscRoomSupplyCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscStatuesCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
    connect(ui->miscTestTileCheckBox, &QCheckBox::released, this, &MainWindow::calculateTickedTileTypes);
}

void MainWindow::miscNoneToggled() {
    if (ui->miscNoneCheckBox->isChecked()) {
        ui->miscPlayerCheckBox->setChecked(false);
        ui->miscCollectablesCheckBox->setChecked(false);
        ui->miscDestructablesCheckBox->setChecked(false);
        ui->miscGatewaysCheckBox->setChecked(false);
        ui->miscElevatorsCheckBox->setChecked(false);
        ui->miscRoomMapCheckBox->setChecked(false);
        ui->miscRoomSaveCheckBox->setChecked(false);
        ui->miscRoomSupplyCheckBox->setChecked(false);
        ui->miscStatuesCheckBox->setChecked(false);
        ui->miscTestTileCheckBox->setChecked(false);
    }
    else {
        ui->miscPlayerCheckBox->setChecked(true);
        ui->miscCollectablesCheckBox->setChecked(true);
        ui->miscDestructablesCheckBox->setChecked(true);
        ui->miscGatewaysCheckBox->setChecked(true);
        ui->miscElevatorsCheckBox->setChecked(true);
        ui->miscRoomMapCheckBox->setChecked(true);
        ui->miscRoomSaveCheckBox->setChecked(true);
        ui->miscRoomSupplyCheckBox->setChecked(true);
        ui->miscStatuesCheckBox->setChecked(true);
        ui->miscTestTileCheckBox->setChecked(true);
    }

    calculateTickedTileTypes();
}

void MainWindow::calculateTickedTileTypes() {
    int activeZones = (int)TileType::none;

    if (ui->zoneGeneralCheckBox->isChecked()) {
        activeZones += (int)TileType::general;
    }
    if (ui->zoneSpaceColonyCheckBox->isChecked()) {
        activeZones += (int)TileType::space_colony;
    }
    if (ui->zoneCrateriaCheckBox->isChecked()) {
        activeZones += (int)TileType::crateria;
    }
    if (ui->zoneBrinstarCheckBox->isChecked()) {
        activeZones += (int)TileType::brinstar;
    }
    if (ui->zoneNorfairCheckBox->isChecked()) {
        activeZones += (int)TileType::norfair;
    }
    if (ui->zoneWreckedShipCheckBox->isChecked()) {
        activeZones += (int)TileType::wreckedShip;
    }
    if (ui->zoneMaridiaCheckBox->isChecked()) {
        activeZones += (int)TileType::maridia;
    }
    if (ui->zoneTourianCheckBox->isChecked()) {
        activeZones += (int)TileType::tourian;
    }

    if (ui->miscDestructablesCheckBox->isChecked()) {
        activeZones += (int)TileType::destructables;
    }
    if (ui->miscPlayerCheckBox->isChecked()) {
        activeZones += (int)TileType::player;
    }
    if (ui->miscCollectablesCheckBox->isChecked()) {
        activeZones += (int)TileType::collectables;
    }
    if (ui->miscGatewaysCheckBox->isChecked()) {
        activeZones += (int)TileType::gateways;
    }
    if (ui->miscElevatorsCheckBox->isChecked()) {
        activeZones += (int)TileType::elevators;
    }
    if (ui->miscRoomMapCheckBox->isChecked()) {
        activeZones += (int)TileType::roomMap;
    }
    if (ui->miscRoomSaveCheckBox->isChecked()) {
        activeZones += (int)TileType::roomSave;
    }
    if (ui->miscRoomSupplyCheckBox->isChecked()) {
        activeZones += (int)TileType::roomSupply;
    }
    if (ui->miscStatuesCheckBox->isChecked()) {
        activeZones += (int)TileType::statues;
    }
    if (ui->miscTestTileCheckBox->isChecked()) {
        activeZones += (int)TileType::testTiles;
    }

    if (activeZones == (int)TileType::none) {
        ui->zoneNoneCheckBox->setCheckState(Qt::CheckState::Checked);
        ui->miscNoneCheckBox->setCheckState(Qt::CheckState::Checked);
    }
    else {
        if ((int)TileType::allZones & activeZones) {
            ui->zoneNoneCheckBox->setCheckState(Qt::CheckState::Unchecked);
        }
        else {
            ui->zoneNoneCheckBox->setCheckState(Qt::CheckState::Checked);
        }
        if ((int)TileType::allMisc & activeZones) {
            ui->miscNoneCheckBox->setCheckState(Qt::CheckState::Unchecked);
        }
        else {
            ui->miscNoneCheckBox->setCheckState(Qt::CheckState::Checked);
        }
    }

    Tile::setTypesToShow(TileType(activeZones));
    invalidate();
}

void MainWindow::btnLoadIconPressed() {
    QString folderPath = "images/";
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Image"), folderPath, tr("Image Files (*.png)"));

    if (fileName.isEmpty()) return;

    if (tileToInsert) delete tileToInsert;

    tileToInsert = new QImage(fileName);
}

void MainWindow::btnDeleteIconPressed() {
    delete tileToInsert;
    tileToInsert = nullptr;
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    QPoint scenePos = view->mapToScene(event->pos()).toPoint();
    qDebug() << scenePos;

//    testFunction();
//    if (tileToInsert) {
//        setEntireImage(scenePos);
//        return;
//    }

    if (tileToInsert) {
        setTileFromFile(scenePos);
        tileToInsert = nullptr;
    }
    else if (ui->makeBackgroundCheckBox->checkState()) {
        QList<QGraphicsItem *> items = scene->items(scenePos);

        for (QGraphicsItem *item : scene->items(scenePos)) {
            Tile *tile = dynamic_cast<Tile *>(item);
            if (tile) {
                tile->makeBackground();
            }
        }

    }
    else if (ui->targetCheckBox) {
        //TODO stuff
    }
}

void MainWindow::setTileFromFile(const QPoint &clickedOffset) {

    int xOffset = (clickedOffset.x() >> 4) << 4;
    int yOffset = (clickedOffset.y() >> 4) << 4;

    QImage *orgMapImage = new QImage(16, 16, QImage::Format_ARGB32);
    QImage *currentImage = new QImage(16, 16, QImage::Format_ARGB32);

    makeImageFromMapTile(QPoint(xOffset, yOffset), orgMapImage);

    QWidget *localViewport = view->viewport();
    QRect rect = localViewport->rect();
    rect = view->mapToScene(rect).boundingRect().toRect();

    int startX = qCeil(rect.left() / 16.f) << 4;
    int endX = qFloor(rect.right() / 16.f) << 4;
    int startY = qCeil(rect.top() / 16.f) << 4;
    int endY = qFloor(rect.bottom() / 16.f) << 4;

//    startX = 0, startY = 0;
//    endX = worldMap->width(), endY = worldMap->height();

    for (int xOffset = startX; xOffset < endX; xOffset += 16) {
        for (int yOffset = startY; yOffset < endY; yOffset += 16) {
            QPoint scenePos(xOffset, yOffset);

            for (QGraphicsItem *item : scene->items(scenePos)) {
                if (dynamic_cast<Tile *>(item)) continue;
            }

            makeImageFromMapTile(scenePos, currentImage);

            if (areSame(orgMapImage, currentImage)) {
                changeTileOfWorldmapFromImage(QPoint(xOffset, yOffset));
            }
        }
    }

    invalidate();
}

void MainWindow::setEntireImage(const QPoint &scenePos) {
    int sceneX = ((scenePos.x() >> 4) << 4);
    int sceneY = (scenePos.y() >> 4) << 4;

    for (int offsetX = 0; offsetX < tileToInsert->width(); ++offsetX) {
        for (int offsetY = 0; offsetY < tileToInsert->height(); ++offsetY) {
            QColor newPixel = tileToInsert->pixelColor(offsetX, offsetY);
            if (newPixel.alpha() == 0) continue;

            worldMap->setPixelColor(sceneX + offsetX, sceneY + offsetY, newPixel);
        }
    }

    invalidate();
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event) {
    switch( event->type() ) {
    case QEvent::MouseButtonPress: {

        QMouseEvent *e = static_cast<QMouseEvent *>(event);
        mousePressEvent(e);
        break;
    }
    case QEvent::MouseButtonDblClick:

        break;
    case QEvent::MouseButtonRelease:

        break;
    default:
        break;
    }

    return QMainWindow::eventFilter(watched, event);
}

void MainWindow::makeBackground() {
    for (auto *item : scene->selectedItems()) {
        Tile *tile = dynamic_cast<Tile *>(item);
        if (tile) {
            tile->makeBackground();
        }
    }
}

void MainWindow::deleteSelected() {
    for (auto *item : scene->selectedItems()) {
        Tile *tile = dynamic_cast<Tile *>(item);
        if (tile) {
            delete tile;
        }
    }
}

void MainWindow::invalidate() {
    QWidget *localViewport = view->viewport();
    QRect localRect = localViewport->rect();
    QRectF localBoundingRect = view->mapToScene(localRect).boundingRect();

    scene->invalidate(localBoundingRect);
}

void MainWindow::keyPressEvent(QKeyEvent *e) {
    switch (e->key()) {
    case Qt::Key_Plus:
        view->scale(0.9, 0.9);
        break;
    case Qt::Key_Minus:
        view->scale(1.1, 1.1);
        break;
    }
}

QString MainWindow::getFilterName(TileType zone) {
    switch (zone) {
    case TileType::general: return "/zones/general";
    case TileType::space_colony: return "/zones/space_colony";
    case TileType::crateria: return "/zones/crateria";
    case TileType::brinstar: return "/zones/brinstar";
    case TileType::norfair: return "/zones/norfair";
    case TileType::wreckedShip: return "/zones/wrecked_ship";
    case TileType::maridia: return "/zones/maridia";
    case TileType::tourian: return "/zones/tourian";
    case TileType::player: return "/misc/player";
    case TileType::collectables: return "/misc/collectables";
    case TileType::destructables: return "/misc/destructables";
    case TileType::roomMap: return "/misc/room_map";
    case TileType::roomSave: return "/misc/room_save";
    case TileType::roomSupply: return "/misc/room_supply";
    case TileType::elevators: return "/misc/elevators";
    case TileType::gateways: return "/misc/gateways";
    case TileType::statues: return "/misc/statues";
    case TileType::testTiles: return "/misc/test_tiles";
    default: return "";
    }
}

void MainWindow::loadCurrentSprites() {
    QString spritesPath = "images/sprites";
    QString tileTypePath = spritesPath + "/tile_types";

    QVector<QVector<QString>> filePaths;

    // load all zones
    for (TileType tileType = TileType::first; tileType <= TileType::last; tileType = TileType((int)tileType * 2)) {
        filePaths.push_back(QVector<QString>());
        QString tileTypeName = getFilterName(tileType);
        QDirIterator it(tileTypePath + tileTypeName, QDirIterator::Subdirectories);

        while (!it.next().isEmpty()) {

            if (it.fileInfo().suffix() == "png") {
                QString name = it.fileName();
                filePaths.back().push_back(it.filePath());

                int id = it.fileName().split(".").first().toInt();
                highestID = qMax(id, highestID);
            }
        }
    }

    for (QVector<QString> &files : filePaths) {
        QCollator collator;
        collator.setNumericMode(true);

        std::sort(files.begin(), files.end(), collator);
    }

    for (int index = 0; index < filePaths.size(); index++) {
        TileType currentTile = (TileType)pow(2, index);

        QVector<QString> paths = filePaths.at(index);
        for (QString filePath : qAsConst(paths)) {

            QImage *image = new QImage(filePath);
            if (!addUniqueImageInVector(zoneIdImageMap[currentTile], image)) {
                delete image;
            }

        }
    }

}

void MainWindow::loadWorldmapMatrix() {

    //    int imagesHorz = worldMap->width() >> 4;
    //    int imagesVert = worldMap->height() >> 4;

    //    worldmapMatrix.resize(imagesHorz);
    //    for (int x = 0; x < imagesHorz; x++) {
    //        worldmapMatrix[x].resize(imagesVert);
    //    }

    //    QFile file("images/worldmap/matrix.txt");
    //    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    //        return;

    //    QString all = QString(file.readAll());

    //    int y = 0;
    //    for (QString row : all.split("\n")) {

    //        int x = 0;
    //        for (QString value : row.split(",")) {
    //            assert(row.size() < worldMap->width() >> 4);

    //            bool success;
    //            int id = value.trimmed().toInt(&success);
    //            if (success) {

    //                for (auto imageIDPair : zoneIdImageMap) {
    //                    if (id == imageIDPair.first) {
    //                        Tile *tile = new Tile(imageIDPair);
    //                        tile->setPos(x << 4, y << 4);

    //                        worldmapMatrix[x][y] = tile;
    //                        scene->addItem(tile);
    //                        break;
    //                    }
    //                }
    //            }

    //            x++;
    //        }

    //        y++;
    //    }
}

void MainWindow::createPNGBackgroundTile() {
    QImage *backgroundTile = new QImage(16, 16, QImage::Format_ARGB32);

    for (int y = 0; y < backgroundTile->height(); y++) {
        for (int x = 0; x < backgroundTile->width(); x++) {
            backgroundTile->setPixelColor(x, y, QColor(0, 0, 0, 255));
        }
    }

    backgroundTile->save("images/tiles/first.png");
}

void MainWindow::convertToPNG() {

    QImage *temp = new QImage(worldMap->width(), worldMap->height(), QImage::Format_ARGB32);

    for (int y = 0; y < worldMap->height(); y++) {
        for (int x = 0; x < worldMap->width(); x++) {
            temp->setPixelColor(x, y, worldMap->pixelColor(x, y));
        }
    }

    temp->save("images/worldmappng.png");
}


void MainWindow::ripTiles() {
    imageToRip = new QImage("images/sprites/a.png");

    cropImages();

    removeOutOfBoundsImages();
    removeDuplicates();

    saveImages();
}

void MainWindow::ripRight() {
    imageToRip = new QImage("images/sprites/a.png");
    QImage *temp = new QImage(imageToRip->width() / 2, imageToRip->height(), QImage::Format_ARGB32);

    int xOffset = temp->width();

    for (int x = 0; x < temp->width(); x++) {
        for (int y = 0; y < temp->height(); y++) {
            QColor localPixelColor = imageToRip->pixelColor(x + xOffset, y);
            temp->setPixelColor(x, y, localPixelColor);
        }
    }

    delete imageToRip;
    imageToRip = temp;

    cropImages();

    removeOutOfBoundsImages();
    removeDuplicates();

    saveImages();
}

void MainWindow::ripLeft() {
    imageToRip = new QImage("images/sprites/a.png");
    QImage *temp = new QImage(imageToRip->width() / 2, imageToRip->height(), QImage::Format_ARGB32);

    for (int x = 0; x < temp->width(); x++) {
        for (int y = 0; y < temp->height(); y++) {
            QColor localPixelColor = imageToRip->pixelColor(x, y);
            temp->setPixelColor(x, y, localPixelColor);
        }
    }

    delete imageToRip;
    imageToRip = temp;

    cropImages();

    removeOutOfBoundsImages();
    removeDuplicates();

    saveImages();
}

void MainWindow::makeImageOfMapPressed() {

    int heightBegin = 0, heightEnd = worldMap->height(), widthBegin = 0, widthEnd = worldMap->width();
    if (!ui->topboundLineEdit->text().isEmpty()) {
        heightBegin = qFloor(ui->topboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->topboundLineEdit->setText(QString::number(heightBegin));
    }
    if (!ui->bottomboundLineEdit->text().isEmpty()) {
        heightEnd = qCeil(ui->bottomboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->bottomboundLineEdit->setText(QString::number(heightEnd));
    }
    if (!ui->leftboundLineEdit->text().isEmpty()) {
        widthBegin = qFloor(ui->leftboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->leftboundLineEdit->setText(QString::number(widthBegin));
    }
    if (!ui->rightboundLineEdit->text().isEmpty()) {
        widthEnd = qCeil(ui->rightboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->rightboundLineEdit->setText(QString::number(widthEnd));
    }

    QImage *temp = new QImage(16, 16, QImage::Format_ARGB32);

    int xOffset = widthBegin;
    int yOffset = heightBegin;
    for (int x = 0; x < 16; x++) {
        for (int y = 0; y < 16; y++) {
            QColor worldmapColor = worldMap->pixelColor(x + xOffset, y + yOffset);
            temp->setPixelColor(x, y, worldmapColor);

        }
    }

    temp->save("images/single_tile.png");
    delete temp;

}

void MainWindow::loadWorldmap() {
    worldMap = new WorldMap();
    *worldMap = new QImage("images/worldmap/worldmap.png");
    scene->addItem(worldMap);
    ui->worldmapCheckBox->setChecked(true);
}

void MainWindow::matchScreenToWorldmap() {
    QWidget *localViewport = view->viewport();
    QRect rect = localViewport->rect();
    rect = view->mapToScene(rect).boundingRect().toRect();

    ui->topboundLineEdit->setText(QString::number(rect.top()));

    ui->bottomboundLineEdit->setText(QString::number(rect.bottom()));

    ui->leftboundLineEdit->setText(QString::number(rect.left()));

    ui->rightboundLineEdit->setText(QString::number(rect.right()));

    matchTilesToWorldmap();
}

void MainWindow::matchTilesToWorldmap() {

    int heightBegin = 0, heightEnd = worldMap->height(), widthBegin = 0, widthEnd = worldMap->width();
    if (!ui->topboundLineEdit->text().isEmpty()) {
        heightBegin = qFloor(ui->topboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->topboundLineEdit->setText(QString::number(heightBegin));
    }
    if (!ui->bottomboundLineEdit->text().isEmpty()) {
        heightEnd = qCeil(ui->bottomboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->bottomboundLineEdit->setText(QString::number(heightEnd));
    }
    if (!ui->leftboundLineEdit->text().isEmpty()) {
        widthBegin = qFloor(ui->leftboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->leftboundLineEdit->setText(QString::number(widthBegin));
    }
    if (!ui->rightboundLineEdit->text().isEmpty()) {
        widthEnd = qCeil(ui->rightboundLineEdit->text().toUInt() / 16.f) << 4;
        ui->rightboundLineEdit->setText(QString::number(widthEnd));
    }


    TileType activeTiles = activeTileTypes();

    int totalTiles = (widthEnd - widthBegin) >> 4;
    int tilesPerThread = totalTiles / (Producer::getThreadCountMax());

    int currentXBegin = widthBegin;
    int currentXEnd = currentXBegin + ((tilesPerThread + 1) << 4);

    while (Producer::getThreadCount() < Producer::getThreadCountMax()) {
        Producer *producer = new Producer(mutex, tilesToAdd, zoneIdImageMap, worldMap, this);
        producerThreads.push_back(producer);
    }


    for (Producer *producer : qAsConst(producerThreads)) {

        QPoint first(currentXBegin, heightBegin);
        if (currentXEnd >= widthEnd) currentXEnd = widthEnd;
        QPoint second(currentXEnd, heightEnd);

        producer->setupThread(qMakePair(first, second), activeTiles);

        connect(producer, &Producer::newItemAdded, this, &MainWindow::newItemAdded);
        connect(producer, &Producer::threadFinished, this, &MainWindow::threadFinished);

        currentXBegin = currentXEnd;
        currentXEnd = currentXBegin + ((tilesPerThread + 1) << 4);

        producer->start();
    }
}


bool MainWindow::matchSpecificWorldmapTile(TileType tileType, const QPoint &scenePos) {

    for (QImage *image : zoneIdImageMap[tileType]) {
        if (areSame(scenePos, image)) {
            Tile *tile = new Tile(tileType, image, scenePos);
            scene->addItem(tile);
            return true;
        }
    }

    return false;
}

void MainWindow::highlightMatch(const QPoint &scenePos) {
    for (int x = 0; x < 16; x++) {
        for (int y = 0; y < 16; y++) {
            QColor currentColor = worldMap->pixelColor(scenePos.x() + x, scenePos.y() + y);
            currentColor.setAlpha(200);
            worldMap->setPixelColor(scenePos.x() + x, scenePos.y() + y, currentColor);
        }
    }
}

bool MainWindow::areSame(QPoint scenePos, QImage *image) {
    for (int x = 0; x < image->height(); x++) {
        for (int y = 0; y < image->width(); y++) {
            QColor tilePixel = image->pixelColor(x, y);

            if (tilePixel.alpha() == 0) continue;

            QColor worldmapPixel = worldMap->pixelColor(scenePos.x() + x, scenePos.y() + y);
            if (tilePixel != worldmapPixel) {
                return false;
            }
        }
    }
    return true;
}

bool MainWindow::zoneIsActive(TileType zone) {
    switch (zone) {
    case TileType::general: return ui->zoneGeneralCheckBox->isChecked();
    case TileType::space_colony: return ui->zoneSpaceColonyCheckBox->isChecked();
    case TileType::crateria: return ui->zoneCrateriaCheckBox->isChecked();
    case TileType::brinstar: return ui->zoneBrinstarCheckBox->isChecked();
    case TileType::norfair: return ui->zoneNorfairCheckBox->isChecked();
    case TileType::wreckedShip: return ui->zoneWreckedShipCheckBox->isChecked();
    case TileType::maridia: return ui->zoneMaridiaCheckBox->isChecked();
    case TileType::tourian: return ui->zoneTourianCheckBox->isChecked();
    case TileType::player: return ui->miscPlayerCheckBox->isChecked();
    case TileType::collectables: return ui->miscCollectablesCheckBox->isChecked();
    case TileType::destructables: return ui->miscDestructablesCheckBox->isChecked();
    case TileType::gateways: return ui->miscGatewaysCheckBox ->isChecked();
    case TileType::elevators: return ui->miscElevatorsCheckBox->isChecked();    case TileType::roomMap: return ui->miscRoomMapCheckBox->isChecked();
    case TileType::roomSave: return ui->miscRoomSaveCheckBox->isChecked();
    case TileType::roomSupply: return ui->miscRoomSupplyCheckBox->isChecked();
    case TileType::statues: return ui->miscStatuesCheckBox->isChecked();
    case TileType::testTiles: return ui->miscTestTileCheckBox->isChecked();
    default: return false;
    }
}

void MainWindow::cropImages() {
    int imageWidth = 16;
    int imageHeight = 16;

    for (int xOffset = 0; xOffset < imageToRip->width(); xOffset += imageWidth) {
        for (int yOffset = 0; yOffset < imageToRip->height(); yOffset += imageHeight) {

            QImage *currImg = new QImage(imageWidth, imageHeight, QImage::Format_ARGB32);
            for (int y = 0; y < imageHeight; y++) {
                for (int x = 0; x < imageWidth; x++) {
                    QColor color = imageToRip->pixelColor(xOffset + x, yOffset + y);
                    if (color == transparentColor) {
                        color = transparentColorWithAlpha;
                    }

                    currImg->setPixelColor(x, y, color);
                }
            }

            images.push_back(currImg);
        }
    }
}

void MainWindow::removeOutOfBoundsImages() {
    for (int i = 0; i < images.size(); i++) {
        QImage * const image = images.at(i);
        if (isOutOfBoundsTile(image)) {
            images.removeAt(i);
            delete image;
            i--;
        }
    }
}

bool MainWindow::isOutOfBoundsTile(QImage *image) {
    for (int y = 0; y < image->height(); y++) {
        for (int x = 0; x < image->width(); x++) {

            QColor pixel = image->pixelColor(x, y);
            if (pixel != QColor(0, 0, 0) && pixel.alpha() != 0) {
                return false;
            }
        }
    }
    return true;
}

bool MainWindow::isOutOfBoundsTile(const QPoint &scenePos) {
    for (int y = 0; y < 16; y++) {
        for (int x = 0; x < 16; x++) {
            QColor pixelColor = worldMap->pixelColor(scenePos.x() + x, scenePos.y() + y);
            if (pixelColor != QColor(0, 0, 0)) {
                return false;
            }
        }
    }
    return true;
}

bool MainWindow::areDuplicates(QImage *src, QImage *dest) {
    assert(src->width() == dest->width() && src->height() == dest->height());

    for (int y = 0; y < src->height(); y++) {
        for (int x = 0; x < src->width(); x++) {
            if (src->pixelColor(x, y) != dest->pixelColor(x, y)) {
                return false;
            }
        }
    }
    return true;
}

void MainWindow::removeDuplicates() {
    loadCurrentSprites();

    for (int i = 0; i < images.size() - 1; i++) {
        QImage *src = images.at(i);

        for (int next = i + 1; next < images.size(); next++) {
            QImage *dest = images.at(next);
            if (areDuplicates(src, dest)) {
                images.removeAt(next);
                delete dest;
                next--;
            }
        }
    }

    // remove duplicates that exist in zoneIdImageMap
    for (TileType zone : zoneIdImageMap.keys()) {

        for (QImage *src : zoneIdImageMap[zone]) {

            for (int i = 0; i < images.size(); i++) {
                QImage *dest = images.at(i);

                if (areDuplicates(src, dest)) {
                    images.removeAt(i);
                    delete dest;
                }
            }
        }
    }
}

bool MainWindow::areSame(QImage *src, QImage *dest) {
    assert(src->width() == dest->width() && src->height() == dest->height());

    for (int y = 0; y < src->height(); y++) {
        for (int x = 0; x < src->width(); x++) {
            QColor srcPixel = src->pixelColor(x, y);
            if (srcPixel != dest->pixelColor(x, y)) {
                return false;
            }
        }
    }
    return true;
}

void MainWindow::changeTileOfWorldmapFromImage(const QPoint &scenePos) {
    for (int x = 0; x < tileToInsert->width(); x++) {
        for (int y = 0; y < tileToInsert->height(); y++) {
            QColor tilePixel = tileToInsert->pixelColor(x, y);

            if (tilePixel.alpha() == 0) continue;

            worldMap->setPixelColor(scenePos.x() + x, scenePos.y() + y, tilePixel);
        }
    }
}

void MainWindow::saveWorldmapChangesPushed() {
    worldMap->save("images/worldmap/worldmap.png");
}

void MainWindow::makeImageFromMapTile(const QPoint &scenePos, QImage *image) {
    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            QColor mapColor = worldMap->pixelColor(scenePos.x() + x, scenePos.y() + y);
            image->setPixelColor(x, y, mapColor);
        }
    }
}

void MainWindow::saveImages() {

    QString path = "images/sprites/captured/";
    QStringList tileFileNames = getTileFileNames(path);

    if (tileFileNames.size()) {
        highestID = qMax(highestID, tileFileNames.last().split(".").first().trimmed().toInt());
    }

    for (auto image = images.begin(); image != images.end(); image++) {
        (*image)->save(path + QString::number(++highestID) + ".png");
        delete *image;
    }

    images.clear();
}

QStringList MainWindow::getTileFileNames(QString path) {
    QDir directory(path);
    QStringList files = directory.entryList(QStringList() << "*.png",QDir::Files);

    QCollator collator;
    collator.setNumericMode(true);

    std::sort(files.begin(), files.end(), collator);

    return files;
}

void MainWindow::saveMatrix() {

    //    QString str;

    //    assert(worldmapMatrix.size());
    //    int mapWidth = worldmapMatrix.size();

    //    assert(worldmapMatrix.first().size());
    //    int mapHeight = worldmapMatrix.first().size();

    //    int digits = 4;

    //    for (int y = 0; y < mapHeight; ++y) {
    //        str += "\n";

    //        for (int x = 0; x < mapWidth; ++x) {
    //            Tile * const tile = worldmapMatrix.at(x).at(y);
    //            if (tile) {
    //                QString numb = QString::number(tile->getId());
    //                while (numb.size() < digits) {
    //                    numb = " " + numb;
    //                }
    //                str += numb + ",";
    //            }
    //            else {
    //                str += "    ,";
    //            }
    //        }
    //        str = str.remove(str.size() - 1, 1);
    //    }

    //    str = str.remove(0, 1);

    //    QFile file("images/worldmap/matrix.txt");
    //    if (!file.open(QFile::OpenModeFlag::ReadWrite)) {
    //        return;
    //    }

    //    file.write(str.toUtf8());
}

void MainWindow::showWorldmap(bool show) {
    worldMap->setVisible(show);
}

void MainWindow::makeBlack(bool value) {
    Tile::makeBlack(value);
    invalidate();
}

TileType MainWindow::activeTileTypes() {
    int activeZones = 0;
    if (ui->zoneGeneralCheckBox->isChecked()) activeZones += (int)TileType::general;
    if (ui->zoneSpaceColonyCheckBox->isChecked()) activeZones += (int)TileType::space_colony;
    if (ui->zoneCrateriaCheckBox->isChecked()) activeZones += (int)TileType::crateria;
    if (ui->zoneBrinstarCheckBox->isChecked()) activeZones += (int)TileType::brinstar;
    if (ui->zoneNorfairCheckBox->isChecked()) activeZones += (int)TileType::norfair;
    if (ui->zoneWreckedShipCheckBox->isChecked()) activeZones += (int)TileType::wreckedShip;
    if (ui->zoneMaridiaCheckBox->isChecked()) activeZones += (int)TileType::maridia;
    if (ui->zoneTourianCheckBox->isChecked()) activeZones += (int)TileType::tourian;
    if (ui->miscPlayerCheckBox->isChecked()) activeZones += (int)TileType::player;
    if (ui->miscCollectablesCheckBox->isChecked()) activeZones += (int)TileType::collectables;
    if (ui->miscDestructablesCheckBox->isChecked()) activeZones += (int)TileType::destructables;
    if (ui->miscGatewaysCheckBox->isChecked()) activeZones += (int)TileType::gateways;
    if (ui->miscElevatorsCheckBox->isChecked()) activeZones += (int)TileType::elevators;
    if (ui->miscRoomMapCheckBox->isChecked()) activeZones += (int)TileType::roomMap;
    if (ui->miscRoomSaveCheckBox->isChecked()) activeZones += (int)TileType::roomSave;
    if (ui->miscRoomSupplyCheckBox->isChecked()) activeZones += (int)TileType::roomSupply;
    if (ui->miscStatuesCheckBox->isChecked()) activeZones += (int)TileType::statues;
    if (ui->miscTestTileCheckBox->isChecked()) activeZones += (int)TileType::testTiles;

    return (TileType)activeZones;
}


