#ifndef GLOBALS_H
#define GLOBALS_H

#include <QGraphicsItem>

enum class TileType {
    none = 0,

    first = 1,

    firstOfZones = first,
    general = firstOfZones,
    space_colony = general * 2,
    crateria = space_colony * 2,
    brinstar = crateria * 2,
    norfair = brinstar * 2,
    wreckedShip = norfair * 2,
    maridia = wreckedShip * 2,
    tourian = maridia * 2,
    lastOfZones = tourian,

    firstOfMisc = lastOfZones * 2,
    player = firstOfMisc,
    collectables = player * 2,
    destructables = collectables * 2,
    roomMap = destructables * 2,
    roomSave = roomMap * 2,
    roomSupply = roomSave * 2,
    elevators = roomSupply * 2,
    gateways = elevators * 2,
    statues = gateways * 2,
    testTiles = statues * 2,
    lastOfMisc = testTiles,

    last = lastOfMisc,

    all = (last * 2) - 1,

    allZones = (lastOfZones * 2) - 1,
    allMisc = (lastOfMisc * 2) - firstOfMisc

};

enum MyItemTypes {
    TileItem = QGraphicsItem::UserType * 2,
    WorldMapItem = TileItem * 2
};


#endif // GLOBALS_H
